const admin = require('firebase-admin');

var config = {
    databaseURL: "https://test-1b62c.firebaseio.com",
    credential: admin.credential.applicationDefault(),
};

admin.initializeApp(config);

const coreNLP = require('./modules/core-nlp/module');
const webhose = require('./modules/webhose/module');

const functions = require('firebase-functions');

exports.updateSearch = functions.database.ref('searches/{searchId}')
    .onUpdate(event => {
        // Exit when the data is deleted.
        if (!event.data.exists()) {
            return null;
        }
        const search = event.data.val();
        if (!search || !search.pending) {
            return null;
        }
        else {
            return webhose.search(search)
                .then(response => {
                    if (response.posts) {
                        const articleIds = response.posts.map(post => {
                            post.searches = {
                                [search.id]: true
                            };
                            admin.database().ref('articles/' + post.uuid)
                                .update(post);
                            return post.uuid;
                        });
                        return event.data.ref.update({ articles: articleIds, pending: false });
                    }
                    else {
                        return null;
                    }
                })
                .catch(err => {
                    console.error(err);
                    return err;
                });
        }
    });

exports.createArticle = functions.database.ref('articles/{articleId}')
    .onCreate(event => {
        var newArticle = event.data.val();
        return coreNLP.document.annotate(newArticle.text)
            .then(annotations => {
                return event.data.ref.update(annotations)
                    .then(res => {
                        console.log('article annotated', res);
                        return res;
                    })
                    .catch(err => {
                        console.log('article annotation err', err);
                        return err;
                    });
            })
            .catch(err => {
                console.error(err);
                return err;
            });
    });
