
describe('Webhose', function() {
    require('../modules/webhose/search.unit');
});

describe('CoreNLP', function() {
    require('../modules/core-nlp/cluster.unit');
    require('../modules/core-nlp/document.unit');
});

describe('Search', function() {
    require('../modules/search/unit');
});