module.exports = {
    json: true,
    qs: {
        size: 10,
        sort: 'relevancy',
        order: 'desc',
        filters: [
            { name: 'site_category', value: 'law_government_and_politics' },
            { name: 'domain_rank', value: '999', equality: '<' },
            { name: 'performance_score', value: '5', equality: '>' },
            { name: 'spam_score', value: 0.7, equality: '<' },
            { name: 'is_first', value: true },
            { name: 'site_type', value: 'news' },
            { name: 'language', value: 'english' },
        ]
    }
};
