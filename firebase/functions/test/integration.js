describe('Webhose', function() {
    require('../modules/webhose/search.integration');
});

describe('CoreNLP', function() {
    require('../modules/core-nlp/document.integration');
    require('../modules/core-nlp/cluster.integration');
});

describe('Search', function() {
    require('../modules/search/integration');
});