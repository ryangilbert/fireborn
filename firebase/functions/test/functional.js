const assert = require('assert');
const config = require('../config');
const rp = require('request-promise');

module.exports = describe('Fully networked', function() {

    var query = {
        size: 100,
        sort: 'relevancy',
        order: 'desc',
        filters: [
            { name: 'site_category', value: 'law_government_and_politics' },
            { name: 'domain_rank', value: '999', equality: '<' },
            { name: 'performance_score', value: '5', equality: '>' },
            { name: 'spam_score', value: 0.5, equality: '<' },
            { name: 'is_first', value: true },
            { name: 'site_type', value: 'news' },
            { name: 'language', value: 'english' },
        ]
    };

    var search;

    before(function(done) {
        const req = {
            url: config.firebase.functions.search,
            body: query,
            method: 'POST',
            timeout: 1000 * 60 * 9,
            forever: true,
            pool: { maxSockets: 8 },
            json: true,
        };
        rp(req)
            .then(res => {
                console.log(res);
                search = res;
                done();
            })
            .catch(err => {
                console.log(err);
                done(err);
            });
    });

    it('HTTP GET /search', function() {
        assert(search.query);
        assert(search.webhose);
        assert(search.corenlp);
    });

});
