const assert = require('assert');
const document = require('./document');

describe('corenlp.document', function() {

    describe('custom annotators', function() {

        const mockDocument = {
            text: 'Jack Bauer said "hi." John said "hello." Jack Bauer said "bye!"',
            quotes: [
                { canonicalSpeaker: 'Jack Bauer', text: '"hi."' },
                { canonicalSpeaker: 'John', text: '"hello."' },
                { canonicalSpeaker: 'Jack Bauer', text: '"bye!"' }
            ],
            entitymentions: [{ text: 'Jack Bauer', ner: 'PERSON' }],
            corefs: {
                "2": [
                    { id: 0, isRepresentativeMention: true, text: 'Jack Bauer' },
                    { id: 1, isRepresentativeMention: false, text: 'Jack Bauer' }
                ]
            }
        };

        it('customAnnotators.customCoref', function() {

            const annotatedDocument = document.customCoref(mockDocument);
            assert(annotatedDocument.corefs['Jack Bauer']);

        });

        it('customAnnotations.speakers', function() {

            const speakers = document.speakers(mockDocument);
            assert(speakers.speakers[0] === 'Jack Bauer');
            assert(speakers.speakers.length === 2);
        });

        it('customAnnotations.aggregate', function() {
            const sentenceAnnotations = ['entitymentions', 'kbp'];
            const mockDocument = {
                sentences: [{
                    entitymentions: [{ test: true }],
                    kbp: [{ test: true }],
                    sentiment: [{ test: true }]
                }]
            };

            const annotatedDocument = document.aggregate(mockDocument);
            // set fake doc annotator data
            sentenceAnnotations.forEach(annotation => {
                assert(annotatedDocument[annotation][0].test);
            });
        });


        it('customAnnotations.people', function() {
            const annotatedDocument = document.people(mockDocument);
            assert(annotatedDocument.people[0].text === 'Jack Bauer');
        });

        it('document.customAnnotations', function() {

            var mockDocument = {
                sentences: [],
                quotes: [],
                entitymentions: [],
                coref: [],
            };
            
            const annotatedDocument = document.customAnnotations( mockDocument);

            assert(annotatedDocument);

        });

    });

});
