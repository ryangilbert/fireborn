const assert = require('assert');

const coreNLP = require('./module');

module.exports = describe('corenlp.cluster', function() {

    var mockWebhoseResults = {
        posts: [{ text: 'Barack Obama decided to get lunch. He said "I want pizza."' }],
        totalResults: 1,
        requestsLeft: 2
    };

    const search = {
        webhose: mockWebhoseResults,
    };

    var clusterResponse;

    describe('cluster.annotate', function() {

        before(function(done) {
            coreNLP.cluster.annotate(search)
                .then(function(response) {
                    clusterResponse = response.corenlp;
                    done();
                })
                .catch(function(err) {
                    done(err);
                });
        });

        it('documents', function() {
            assert(clusterResponse.documents[0]);
        });

        it('cluster', function() {
            assert(clusterResponse.cluster);
        });

    });

    describe('cluster.customAnnotations', function() {

        before(function(done) {
            coreNLP.cluster.annotate(search)
                .then(function(response) {
                    clusterResponse = response.corenlp;
                    done();
                })
                .catch(function(err) {
                    done(err);
                });
        });

        it('aggregate', function() {
            const customAnnotations = [
                'corefs', 'quotes', 'entitymentions', 'speakers', 'people'
            ];
            customAnnotations.forEach(annotation => {
                const cluster = clusterResponse.cluster[annotation];
                const annotationData = cluster[0].data;
                if (Array.isArray(annotationData)) {
                    assert(annotationData[0]);
                }
                else {
                    assert(annotationData);
                }
            });
        });

    });

});
