const assert = require('assert');

module.exports = describe('HighlightService', function() {

    var ngMock = window.angular.mock;

    var highlightService;

    beforeEach(function() {
        window.angular.module('firebase', []);
        window.angular.module('ngCsv', []);
        window.angular.module('ngRoute', []);
        window.angular.module('ngSanitize', []);

        ngMock.module('search');
        ngMock.inject(function(HighlightService) {
            highlightService = HighlightService;
        });
    });

    // tests ordered by reverse method declaration

    describe('highlightPosts', function() {

        var posts = [{
            text: 'John Smith is a person. He said "hi."',
            entities: { speakers: [{ name: 'John Smith' }] },
            quotes: [{ text: '"hi."', postId: 0 }]
        }];
        const filters = [{ name: 'text', value: 'person' }];
        var highlightedPosts = highlightService.highlightPosts(posts, filters);

        it('should return an array of post objects', function() {
            assert(Array.isArray(highlightedPosts));
            assert(highlightedPosts[0]);
            assert(highlightedPosts[0].quotes);
            assert(highlightedPosts[0].entities);
            assert(highlightedPosts[0].text);
        });

        it('should return highlighted text in array of posts', function() {
            assert(highlightedPosts[0].text.indexOf('<span class="highlight-entities">John Smith</span>') > -1);
            assert(highlightedPosts[0].text.indexOf('<span class="highlight-quotes">"hi."</span>') > -1);
            assert(highlightedPosts[0].text.indexOf('<span class="highlight-search">person</span>') > -1);
        });

    });

    describe('getFilters', function() {
        it('should return a filter object with regex values', function() {
            const filters = highlightService.getFilters([{
                name: 'text',
                value: 'test'
            }]);
            assert(filters[0] == 'test');
        });
    });

    describe('getEntities', function() {
        it('should return an array of entities with regex values', function() {
            const entities = highlightService.getEntities({ speakers: [{ name: 'test' }] });
            assert(entities[0] === 'test');
        });
    });

    describe('getQuotes', function() {
        it('should return an array of post quotes', function() {
            const postQuotes = highlightService.getQuotes([{ text: 'test', postId: 0 }], 0);
            assert(postQuotes[0]);
            assert(postQuotes[0] === 'test');
        });
    });

    describe('highlightValue', function() {
        const highlightedValue = highlightService.highlightValue('entities', 'test person', 'Test Person is hungry *"\\/).');
        it('should return a highlighted value', function() {
            const highlighted = '<span class="highlight-entities">Test Person</span> is hungry *"\\/).';
            assert(highlightedValue === highlighted);
        });
    });

    describe('highlightPost', function() {
        const highlightedPost = highlightService.highlightPost([
            { search: ['test'] },
            { quotes: ['"hello."'] },
            { entities: ['John Smith'] },
        ], { text: 'test', value: 'test' });
        it('should return an post with highlights', function() {
            assert(highlightedPost.text.indexOf('test') > -1);
        });
    });

});
