exports.customAnnotations = customAnnotations;
exports.aggregate = aggregate;

function customAnnotations(corenlp) {
    var annotatedResponse = Object.assign({}, corenlp);
    const customAnnotators = ['aggregate'];
    customAnnotators.forEach(annotation => {
        annotatedResponse = exports[customAnnotators](annotatedResponse);
    });
    return annotatedResponse;
}

function aggregate(corenlp) {

    if (corenlp.cluster && corenlp.documents) {

        const documentAnnotations = [
            'corefs', 'quotes', 'entitymentions', 'kbp', 'speakers', 'people'
        ];

        corenlp.documents.forEach(doc => {
            documentAnnotations.forEach(annotation => {
                const cluster = corenlp.cluster;
                if (!cluster[annotation]) {
                    cluster[annotation] = [];
                }
                if (doc[annotation]) {
                    const annotationData = {
                        documentId: doc.docDate,
                        data: doc[annotation]
                    };
                    if (!Array.isArray(doc[annotation])) {
                        cluster[annotation].push(annotationData);
                    }
                    else {
                        const combinedAnnotations = cluster[annotation].concat(annotationData);
                        cluster[annotation] = combinedAnnotations;
                    }
                }
            });
        });

    }

    return corenlp;
}
