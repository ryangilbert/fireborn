const assert = require('assert');

const coreNLP = require('./module');

module.exports = describe('corenlp.document', function() {

    var mockWebhoseResults = {
        posts: [{ text: 'Barack Obama decided to get lunch. He said "I want pizza."' }],
        totalResults: 1,
        requestsLeft: 2
    };

    describe('document.annotate', function() {

        var annotations;

        before(function(done) {
            const articleText = mockWebhoseResults.posts[0].text;
            const pool = { maxSockets: 1 };
            coreNLP.document.annotate(articleText, pool)
                .then(function(res) {
                    annotations = res;
                    done();
                })
                .catch(function(err) {
                    done(err);
                });
        });

        it('corefs', function() {
            const corefs = annotations.corefs;
            const corefKeys = Object.keys(annotations.corefs);
            const firstEntity = corefs[corefKeys[0]];
            const firstCoref = firstEntity[0];
            const secondCoref = firstEntity[1];
            assert(firstCoref.text === "Barack Obama");
            assert(firstCoref.isRepresentativeMention);
            assert(secondCoref.text === "He");
            assert(!secondCoref.isRepresentativeMention);
        });

        it('quotes', function() {
            const firstQuote = annotations.quotes[0];
            assert(firstQuote.canonicalSpeaker === 'Barack Obama');
            assert(firstQuote.text === '"I want pizza."');
            assert(firstQuote.beginSentence === 1);
            assert(firstQuote.endSentence === 1);
        });

        it('entitymentions', function() {
            const firstSentence = annotations.sentences[0];
            const firstEntity = firstSentence.entitymentions[0];
            assert(firstEntity.text === 'Barack Obama');
            assert(firstEntity.ner === 'PERSON');
            assert(firstEntity.entitylink === 'Barack_Obama');
        });

    });

    describe('document.customAnnotations', function() {

        var customAnnotations;

        before(function(done) {
            const pool = { maxSockets: 1 };
            coreNLP.document.annotate(mockWebhoseResults.posts[0].text, pool)
                .then(function(res) {
                    customAnnotations = res;
                    done();
                })
                .catch(function(err) {
                    done(err);
                });
        });

        it('returns document', function() {
           assert(customAnnotations); 
        });
        
        it('customCoref', function() {
            assert(customAnnotations.corefs['Barack Obama']);
        });

        it('speakers', function() {
            assert(customAnnotations.speakers.length === 1);
            assert(customAnnotations.speakers[0] === "Barack Obama");
            assert(customAnnotations.kbp);
            // assert(customAnnotations.sentiment); // sentiment currently removed on doc
        });

        it('aggregate', function() {
            assert(customAnnotations.entitymentions[0]);
            // assert(annotatedDocument.sentiment[0]);  // sentiment currently removed on doc
            // todo: add more assertions when i find out exactly what entitymentions provides
        });

    });

});
