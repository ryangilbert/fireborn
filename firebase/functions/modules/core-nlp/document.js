const config = require('../../config');
const coreNLPServer = config.coreNLPServer;
const rp = require('request-promise');

exports.annotate = annotate;
exports.customAnnotations = customAnnotations;
exports.customCoref = customCoref;
exports.speakers = speakers;
exports.aggregate = aggregate;
exports.people = people;

function annotate(text) {

    const annotators = [
        'tokenize',
        'ssplit',
        //'truecase', // not currently used, causes exception
        'pos',
        'lemma',
        'ner', // required for coref
        'depparse', // required for quote
        'parse', // required for neural coref
        'gender',
        'entitymentions', // currently only shows up in sentences
        'natlog', // required by quote
        'coref',
        'openie', // required for accurate speaker quoting
        //'kbp', // not needed for data or entitylink right now, taking out temporarily to improve performance
        'entitylink', // links entities to wikipedia 
        //'sentiment', // only shows up in sentences currently
        'quote.attribution',
        'quote',
    ];

    const annotatorString = annotators.reduce((accumulator, currentValue, index, array) => {
        const lastItem = index === array.length;
        var comma = lastItem ? '' : ',';
        return accumulator += comma += currentValue;
    });

    var qs = {
        // JSON required for URL encoding
        properties: JSON.stringify({
            date: new Date(),
            annotators: annotatorString,
            prettyPrint: false,
            quiet: true,
            timeout: 1000 * 60 * 9,
            'entitymentions.acronyms': true,
            //'coref.verbose': true,
            'coref.algorithm': 'statistical', // statistical is faster
            'parse.model': 'edu/stanford/nlp/models/srparser/englishSR.ser.gz', // shift reduce
            //'parse.nthreads': 1,
            //'parse.verbose': true,
            // 'quote.verbose': true,
            //'quote.attribution.verbose': true,
            // 'truecase.overwriteText': true,
            //'tokenize.verbose': true,
            // 'quote.singleQuotes': 'false',
            // 'quote.smartQuotes': true,
            // 'quote.asciiQuotes': true,
            // 'quote.attribution.MSSieves': 'det,top,loose,maj',
        }),
    };

    const annotationRequest = {
        url: coreNLPServer.url,
        qs: qs,
        form: text,
        auth: {
            user: config.coreNLPServer.user,
            password: config.coreNLPServer.password
        },
        forever: true,
        pool: config.coreNLPServer.pool,
        simple: false,
        transform: customAnnotations,
        transform2xxOnly: true,
    };

    return rp.post(annotationRequest);

}

function customAnnotations(responseBody) {
    var annotatedDocument = JSON.parse(responseBody);
    const documentAnnotators = ['aggregate', 'speakers', 'people'];
    documentAnnotators.forEach(annotation => {
        annotatedDocument = exports[annotation](annotatedDocument);
    });
    return annotatedDocument;
}

function customCoref(annotations) {
    if (annotations.corefs) { 
        // change arbitrary coref keys to representative mention name
        for (var corefKey in annotations.corefs) {
            var corefs = annotations.corefs[corefKey];
            corefs.forEach(coref => {
                if (coref.isRepresentativeMention) {
                    annotations.corefs[coref.text] = corefs;
                    delete annotations.corefs[corefKey];
                }
            });
        }
    }
    return annotations;
}

function aggregate(annotatedDocument) {

    const sentenceAnnotations = ['entitymentions'];
    annotatedDocument.sentences.forEach(sentence => {
        sentenceAnnotations.forEach(annotation => {
            if (!annotatedDocument[annotation]) {
                annotatedDocument[annotation] = [];
            }
            const docAnnotations = annotatedDocument[annotation]
                .concat(sentence[annotation]);
            annotatedDocument[annotation] = docAnnotations;
        });

    });

    return annotatedDocument;

}

function speakers(annotatedDocument) {

    // requires aggregate annotator
    // order speakers by number of quotes 

    var speakerNames = {};
    const speakers = annotatedDocument.quotes
        .map(quote => {
            // get array of speaker names
            const speaker = quote.canonicalSpeaker || quote.speaker;
            if (!speakerNames[speaker]) {
                // set speaker quote count to 0
                speakerNames[speaker] = 1;
                return speaker;
            }
            else {
                // increment quote count
                speakerNames[speaker]++;
                return null;
            }
        })
        .filter(speaker => {
            // remove undefined speakers
            return speaker !== null;
        })
        .sort((speakerA, speakerB) => {
            // sort speakers by quote count
            const speakerAQuotes = speakerNames[speakerA];
            const speakerBQuotes = speakerNames[speakerB];
            return speakerBQuotes - speakerAQuotes;
        });

    annotatedDocument.speakers = speakers;
    return annotatedDocument;

}

function people(annotatedDocument) {
    if (annotatedDocument.entitymentions) {
        annotatedDocument.people = annotatedDocument.entitymentions
            .filter(entity => {
                return entity.ner === 'PERSON';
            });
    }
    return annotatedDocument;
}
