/* global angular */

angular.module('search')
    .service('HighlightService', [HighlightService]);

function HighlightService() {

    var Highlight = {};

    Highlight.highlightPosts = highlightPosts;
    Highlight.getFilters = getFilters;
    Highlight.getEntities = getEntities;
    Highlight.getQuotes = getQuotes;
    Highlight.highlightPost = highlightPost;
    Highlight.highlightValue = highlightValue;

    function highlightPosts(results) {
        var posts = results.webhose.posts;
        var searchHighlights = getFilters(results.filters);
        angular.forEach(posts, function(post, postIndex) {
            // ordered by importance (can't overrwrite highlight for now)
            var annotations = results.corenlp.documents[postIndex];
            var quotes = annotations.quotes;
            var entities = annotations.entitymentions;
            var quoteHighlights = Highlight.getQuotes(quotes, postIndex);
            var entitityHighlights = Highlight.getEntities(entities);
            console.log(entitityHighlights);
            var highlights = [
                { entities: entitityHighlights },
                { quotes: quoteHighlights },
                { search: searchHighlights },
            ];
            post.highlightedText = Highlight.highlightPost(highlights, post);
        });
    }

    function getFilters(filters) {

        // highlight search values

        var highlightValues = [];

        if (Array.isArray(filters) && filters[0]) {
            filters.forEach(function(filter) {
                var highlightedFilters = ['text', 'person', 'organization'];
                var highlightFilter = highlightedFilters.indexOf(filter.name) > -1;
                if (highlightFilter) {
                    highlightValues.push(filter.value);
                }
            });
        }

        return highlightValues;
    }

    function getEntities(postEntities) {

        var highlightValues = [];

        postEntities.forEach(function(entityValue) {
            if (entityValue.entity_link) {
                highlightValues.push(entityValue.entitylink);
            }
        });

        return highlightValues;

    }

    function getQuotes(postQuotes, postIndex) {
        return postQuotes.map(function(quote) {
            return quote.text;
        });
    }

    function highlightPost(highlights, post) {

        // highlights: search, entities, quotes
        highlights.forEach(function(highlightTypes) {
            const highlightType = Object.keys(highlightTypes)[0];
            const highlightValues = highlightTypes[highlightType];
            highlightValues.forEach(function(value) {
                post.text = highlightValue(highlightType, value, post.text);
            });
        });

        return post;

    }

    function highlightValue(highlightType, value, text) {
        var escapedRegex = /[.*+?^${}()|[\]\\]/g;
        var escaped = value.replace(escapedRegex, "\\$&");
        var highlight = '<span class="highlight-' + highlightType + '">$&</span>';
        var highlightRegex = new RegExp(escaped, 'gi');
        return text.replace(highlightRegex, highlight);
    }

    return Highlight;

}
