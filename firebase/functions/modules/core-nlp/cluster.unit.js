const assert = require('assert');
const cluster = require('./cluster');

describe('corenlp.cluster', function() {

    const mockResponse = {
        cluster: {},
        documents: [{ docDate: "2018-02-03T05:14:41.123Z"}],
    };
    
    describe('custom annotators', function() {

        it('customAnnotations.aggregate', function() {

            const documentAnnotations = [
                'corefs', 'quotes', 'entitymentions', 'kbp', 'speakers', 'people'
            ];

            // set fake doc annotator data
            documentAnnotations.forEach(annotation => {
                mockResponse.documents[0][annotation] = [];
            });

            const aggregate = cluster.aggregate;
            const annotatedCluster = aggregate(mockResponse);
            
            documentAnnotations.forEach(annotation => {
                assert(annotatedCluster.cluster[annotation][0].documentId);
                assert(annotatedCluster.cluster[annotation][0].data);
            });
        });

        it('cluster.customAnnotations', function() {
            const annotatedCluster = cluster.customAnnotations(mockResponse);
            assert(annotatedCluster.cluster);
            assert(annotatedCluster.documents[0]);
        });

    });

});
