const webhoseio = require('webhoseio');
const config = require('../../config');
const client = webhoseio.config(config.webhose);

// methods sorted by invocation order

module.exports.search = search;

module.exports.formatQuery = formatQuery;
module.exports.filterString = filterString;
module.exports.mapFilters = mapFilters;
module.exports.parseFilters = parseFilters;
module.exports.parseFilter = parseFilter;

function search(query) {

    // send search to webhose and retrieve results

    const formattedQuery = formatQuery(query);
    console.log('formattedQuery', formattedQuery);
    return client.query('filterWebContent', formattedQuery);

    // returns { posts, totalResults, requestsLeft }

}

function formatQuery(query) {
    var formattedQuery = {
        accuracy_confidence: 'high',
        order: query.order,
        q: filterString(query.filters),
        size: 100,
        sort: query.sort,
        ts: new Date().setDate(-30),
    };
    for (var queryProperty in formattedQuery) {
        var queryValue = formattedQuery[queryProperty];
        var invalid = queryValue === '' || queryValue === undefined || queryValue === null;
        if (invalid) {
            delete formattedQuery[queryProperty];
        }
    }
    return formattedQuery;
}

function filterString(filters) {

    // format filter object into webhose boolean filter string

    if (Array.isArray(filters) && filters[0]) {

        var filterObject = mapFilters(filters);

        var filter = '';

        for (var filterName in filterObject) {
            var f = filterObject[filterName];
            filter += parseFilters(filterName, f) + ' ';
        }

        return filter.trim();
    }

    else {
        return '';
    }

}


function mapFilters(filters) {

    var filterObject = {};

    filters.forEach(function(filter) {

        if (!filter.name) {
            filter = JSON.parse(filter);
        }

        if (!filterObject[filter.name]) {
            filterObject[filter.name] = [];
        }

        filterObject[filter.name].push(filter);

    });

    return filterObject;

}

function parseFilters(filterName, filter) {

    var filterValues = '';

    var filterLabel = filterName;

    filter.forEach(function(f, i, a) {

        if (filterName === 'entity_sentiment') {
            filterLabel = f.value.entityType + '.' + f.value.sentiment;
        }

        filterValues += parseFilter(f);

        if (a[i + 1]) filterValues += ' ';

    });

    var parsedFilter = `${filterLabel}:(${filterValues})`;

    return parsedFilter;

}

function parseFilter(filter) {

    var f = '';

    if (filter.equality) {
        f += filter.equality;
    }

    if (filter.name === 'published') {
        var dateMs = new Date(filter.value).getTime();
        f += dateMs;
    }

    else if (filter.name === 'entity_sentiment') {
        if (filter.value.entity.indexOf(' ') > -1) {
            filter.value.entity = '"' + filter.value.entity + '"';
        }
        f += filter.value.entity;
    }

    else if (typeof filter.value === 'string') {
        // webhose escaped characters
        var whitespace = filter.value.indexOf(' ') > -1;
        if (whitespace) {
            f += ('"' + filter.value + '"');
        }
        else {
            f += filter.value;
        }
    }

    else {
        // numbers/booleans
        f += filter.value;
    }

    return f;

}
