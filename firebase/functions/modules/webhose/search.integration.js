const assert = require('assert');
const webhose = require('./module');

module.exports = describe('webhose.search', function() {

    var response;

    before(function(done) {
        var query = {
            size: 1,
            order: 'desc',
            sort: 'relevancy',
            filters: [
                { name: 'published', value: new Date(), equality: '<' },
                { name: 'site_category', value: 'tech' },
                { name: 'site_category', value: 'international news', link: ' OR ' },
                { name: 'spam_score', value: 0.8, equality: '<' },
                { name: 'is_first', value: true },
                { name: 'site_type', value: 'news' },
                { name: 'language', value: 'english' },
            ]
        };
        webhose.search(query)
            .then(function(results) {
                response = results;
                if (process.env.debug) {
                    const fs = require('fs');
                    const fileName = 'modules/webhose/data/sample.response.json';
                    const fileData = JSON.stringify(results, null, 4);
                    fs.writeFileSync(fileName, fileData);
                }
                done();
            }, function(err) {
                done(err);
            });
    });

    it('should return a webhose response object', function() {
        assert(response.query);
        assert(typeof response === 'object');
        const webhoseResponse = response.webhose;
        assert(webhoseResponse.hasOwnProperty('posts'));
        assert(webhoseResponse.hasOwnProperty('totalResults'));
        assert(webhoseResponse.hasOwnProperty('requestsLeft'));
        assert(webhoseResponse.hasOwnProperty('next'));
        assert(webhoseResponse.posts[0].uuid);
    });
});
