const assert = require('assert');

const search = require('./search');

module.exports = describe('search.formatQuery', function() {

    function query() {
        return {
            accuracy_confidence: true,
            from: 0,
            highlight: false,
            latest: false,
            order: 'desc',
            size: 100,
            sort: 'relevance',
            filters: [
                { name: 'text', value: 'test' },
                {
                    name: 'entity_sentiment',
                    value: {
                        entityType: 'person',
                        sentiment: 'positive',
                        entity: 'elon musk'
                    }
                },
                { name: 'published', value: new Date() },
                { name: 'site_category', value: 'politics' },
                { name: 'site_category', value: 'international_news', link: ' OR ' },
                { name: 'spam_score', value: 0.5, equality: '<' },
                { name: 'is_first', value: true },
                { name: 'person', value: 'donald trump' },
                { name: 'organization', value: '' },
                { name: 'location', value: '' },
                { name: 'site_type', value: ['news'] },
                { name: 'performance_score', value: 5, equality: '>' },
                { name: 'domain_rank', value: '' },
                { name: 'language', value: 'english' },
                { name: 'thread.country', value: 'USA' },
                { name: 'thread_title', value: 'Celebrity', equality: '!' },
                { name: 'author', value: 'david frum' },
                { name: 'thread_url', value: 'http://google.com' },
                { name: 'external_links', value: 'http://google.com' }
            ],
        };
    }

    describe('formatQuery', function() {
        const formattedQuery = search.formatQuery(new query());

        it('should return an query object', function() {
            assert(typeof formattedQuery === 'object');
        });

        it('return object should contain all query parameters', function() {
            var formattedParams = Object.keys(formattedQuery);
            var queryParams = ['accuracy_confidence', 'from', 'highlight', 'latest', 'order', 'q', 'size', 'sort', 'ts'];
            formattedParams.map(param => {
                assert(queryParams.indexOf(param) > -1);
            });
        });

    });

    describe('filterString', function() {
        var filters = new query().filters;
        var filteredString = search.filterString(filters);

        it('should return a string', function() {
            assert(filteredString);
        });
    });

    describe('mapFilters', function() {
        var filters = new query().filters;
        var mappedFilters = search.mapFilters(filters);

        it('should return an object', function() {
            assert(typeof mappedFilters === 'object');
            assert(!Array.isArray(mappedFilters));
        });

        it('should map filter names to arrays of filter values', function() {
            assert(mappedFilters.hasOwnProperty('text'));
            assert(Array.isArray(mappedFilters.text));
            assert(mappedFilters.text[0]);
            assert(typeof mappedFilters.text[0] === 'object');
            assert(mappedFilters.text[0].hasOwnProperty('value'));
            assert(mappedFilters.text[0].value === filters[0].value);
        });

    });

    describe('parseFilter', function() {
        var filters = new query().filters;

        it('should format/escape string type filters', function() {
            var escapedFilterString = search.parseFilter(filters[0]);
            assert(escapedFilterString === 'test');
        });

        it('should format the entity_sentiment filter', function() {
            var sentimentString = search.parseFilter(filters[1]);
            assert(sentimentString.indexOf('elon musk') > -1);
        });

        it('should format the date filter', function() {
            var dateString = search.parseFilter(filters[2]);
            var dateMS = filters[2].value.getTime();
            assert(dateString.indexOf(dateMS) > -1);
        });

        it('should add links and equality to filter string', function() {
            var webhoseBoolean = search.parseFilter(filters[4]);
            assert(webhoseBoolean.indexOf(' OR ') > -1);
        });

        it('should insert number/boolean types into string', function() {
            var numberValue = search.parseFilter(filters[5]);
            var booleanValue = search.parseFilter(filters[6]);
            assert(numberValue.indexOf('<0.5') > -1);
            assert(booleanValue.indexOf('true') > -1);
        });

    });

    describe('parseFilters', function() {
        var filters = new query().filters;
        var filtered = search.parseFilters(filters[1].name, [filters[1]]);

        it('should return a string', function() {
            assert(typeof filtered === 'string');
        });

        it('should parse sentiment filter object into string', function() {
            assert(filtered.indexOf('person.positive:("elon musk")') > -1);
        });

    });
});
