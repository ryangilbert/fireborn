;(function() {
"use strict";

/* global angular, firebase */

var config = {
apiKey: "AIzaSyBC88KArmN27_iwYmimElbugz-9Y_401JU",
authDomain: "inbound-planet-218422.firebaseapp.com",
databaseURL: "https://inbound-planet-218422.firebaseio.com",
projectId: "inbound-planet-218422",
storageBucket: "inbound-planet-218422.appspot.com",
messagingSenderId: "41168955520"
};

firebase.initializeApp(config);

angular.module('app', ['firebase','templates', 'search']);
}());

;(function() {
"use strict";

/* global angular */

angular.module('search', ['ngCsv', 'ngRoute', 'ngSanitize']);
}());

;(function() {
"use strict";

/* global angular */

angular.module('search')
    .constant('Filters', (Filters()));

function Filters() {

    return {

        sort: [
            'relevancy', 'crawled', 'published', 'thread.published', 'social.facebook.likes', 'social.facebook.shares', 'social.facebook.comments', 'social.gplus.shares', 'social.pinterest.shares', 'social.linkedin.shares', 'social.stumbledupon.shares', 'social.vk.shares', 'replies_count', 'participants_count', 'spam_score', 'performance_score', 'domain_rank', 'ord_in_thread', 'rating'
        ], // sort options

        order: [
            { label: 'ascending', value: 'asc' },
            { label: 'descending', value: 'desc' }
        ], // order opations (label/value)

        links: [
            { label: 'or', value: ' OR ' },
            { label: 'and', value: ' AND ' }
        ], // link options (label/value)

        equality: {
            object: false,
            checkbox: false,
            text: [{ label: 'is', value: '' }, { label: 'is not', value: '!' }],
            number: [{ label: 'equal to', value: '' }, { label: 'not equal to', value: '!' }, { label: 'greater than', value: '>' }, { label: 'less than', value: '<' }],
            date: [{ label: 'on', value: '' }, { label: 'not on', value: '!' }, { label: 'after', value: '>' }, { label: 'before', value: '<' }],
        }, // equality options (label/value)

        filters: {
            text: {
                label: 'article text',
                element: 'input',
                type: 'text'
            },
            entity_sentiment: {
                element: 'sentiment',
                label: 'sentiment',
                type: 'object',
                value: {
                    entityType: '',
                    sentiment: '',
                    entity: '',
                },
                options: {
                    sentiment: ['positive', 'neutral', 'negative'],
                    entity: ['person', 'organization']
                }
            },
            person: {
                element: 'input',
                type: 'text'
            },
            organization: {
                element: 'input',
                type: 'text'
            },
            location: {
                element: 'input',
                type: 'text'
            },
            site_type: {
                element: 'select',
                type: 'text',
                options: [
                    'news', 'blogs', 'discussions'
                ]
            },
            site_category: {
                element: 'select',
                type: 'text',
                options: categories()
            },
            published: {
                element: 'input',
                type: 'date',
                label: 'date published'
            },
            performance_score: {
                label: 'virality score (1-10)',
                min: 0,
                max: 10,
                element: 'input',
                type: 'number'
            },
            domain_rank: {
                element: 'input',
                type: 'number',
                label: 'domain rank (1-1000)',
                min: 1,
                max: 1000
            },
            language: {
                element: 'input',
                type: 'text',
            },
            'thread.country': {
                element: 'input',
                type: 'text',
                label: 'publication country'
            },
            thread_title: {
                element: 'input',
                type: 'text',
                label: 'publication title'
            },
            author: {
                element: 'input',
                type: 'text'
            },
            spam_score: {
                element: 'input',
                type: 'number',
                label: 'spam score (0-1)',
                min: 0,
                max: 1
            },
            is_first: {
                element: 'input',
                type: 'checkbox',
                label: 'ignore comments'
            },
            thread_url: {
                element: 'input',
                type: 'text',
                label: 'url'
            },
            external_links: {
                element: 'input',
                type: 'text',
                label: 'referenced URL'
            },
            // 'social.facebook.likes': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.facebook.shares': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.facebook.comments': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.glplus.shares': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.pinterest.shares': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.linkedin.shares': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.stumbledupon.shares': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // },
            // 'social.vk.shares': {
            //     element: 'input',
            //     type: 'number',
            //     min: 0,
            //     max: 1
            // }
        } // filter options (name: optionsObject)

    };

    function categories() {
        return [
            '3d_graphics',
            '7_12_education',
            'a_d_d',
            'aids_hiv',
            'accessories',
            'adoption',
            'adult_education',
            'adventure_travel',
            'advertising',
            'africa',
            'agriculture',
            'air_travel',
            'allergies',
            'alternative_medicine',
            'alternative_religions',
            'american_cuisine',
            'animation',
            'antivirus_software',
            'apartments',
            'appliances',
            'aquariums',
            'architects',
            'art_history',
            'art_technology',
            'arthritis',
            'arts_and_crafts',
            'asthma',
            'astrology',
            'atheism_agnosticism',
            'australia_and_new_zealand',
            'autism_pdd',
            'auto_parts',
            'auto_racing',
            'auto_repair',
            'vehicles',
            'babies_and_toddlers',
            'barbecues_and_grilling',
            'baseball',
            'beadwork',
            'beauty',
            'bed_and_breakfasts',
            'beginning_investing',
            'bicycling',
            'biology',
            'biotech_biomedical',
            'bipolar_disorder',
            'birds',
            'birdwatching',
            'games',
            'body_art',
            'bodybuilding',
            'books_and_literature',
            'botany',
            'boxing',
            'brain_tumor',
            'buddhism',
            'budget_travel',
            'business',
            'business_software',
            'business_travel',
            'buying_selling_cars',
            'buying_selling_homes',
            'by_us_locale',
            'c_c_plus_plus',
            'cajun_creole',
            'cameras_and_camcorders',
            'camping',
            'canada',
            'cancer',
            'candle_and_soap_making',
            'canoeing_kayaking',
            'car_culture',
            'gambling',
            'career_advice',
            'career_planning',
            'jobs',
            'caribbean',
            'catholicism',
            'cats',
            'celebrity_fan_gossip',
            'cell_phones',
            'certified_pre_owned',
            'cheerleading',
            'chemistry',
            'chess',
            'chinese_cuisine',
            'cholesterol',
            'christianity',
            'chronic_fatigue_syndrome',
            'chronic_pain',
            'cigars',
            'climbing',
            'clothing',
            'cocktails_beer',
            'coffee_tea',
            'cold_and_flu',
            'collecting',
            'college',
            'college_administration',
            'college_life',
            'comic_books',
            'commentary',
            'comparison',
            'computer_certification',
            'computer_networking',
            'computer_peripherals',
            'computer_reviews',
            'construction',
            'contests_and_freebies',
            'convertible',
            'copyright_infringement',
            'coupe',
            'couponing',
            'credit_debt_and_loans',
            'cricket',
            'crossover',
            'cruises',
            'cuisine_specific',
            'data_centers',
            'databases',
            'dating',
            'daycare_pre_school',
            'deafness',
            'dental_care',
            'depression',
            'dermatology',
            'desktop_publishing',
            'desktop_video',
            'desserts_and_baking',
            'diabetes',
            'diesel',
            'dining_out',
            'distance_learning',
            'divorce_support',
            'dogs',
            'drawing_sketching',
            'eastern_europe',
            'education',
            'eldercare',
            'electric_vehicle',
            'email',
            'engines',
            'english_as_a_2nd_language',
            'entertaining',
            'entertainment',
            'environmental_safety',
            'epilepsy',
            'ethnic_specific',
            'europe',
            'exercise',
            'adult',
            'family_and_parenting',
            'family_internet',
            'fashion',
            'figure_skating',
            'financial_aid',
            'financial_news',
            'financial_planning',
            'fine_art',
            'fly_fishing',
            'food',
            'food_allergies',
            'football',
            'forestry',
            'france',
            'freelance_writing',
            'french_cuisine',
            'freshwater_fishing',
            'gerd_acid_reflux',
            'game_and_fish',
            'gardening',
            'gay_life',
            'genealogy',
            'geography',
            'geology',
            'getting_published',
            'golf',
            'government',
            'graduate_school',
            'graphics_software',
            'greece',
            'green_solutions',
            'guitar',
            'hatchback',
            'hate_content',
            'headaches_migraines',
            'health',
            'health_lowfat_cooking',
            'heart_disease',
            'hedge_fund',
            'herbs_for_health',
            'hinduism',
            'hobbies_and_interests',
            'holistic_healing',
            'home_and_garden',
            'home_recording',
            'home_repair',
            'home_theater',
            'home_video_dvd',
            'homeschooling',
            'homework_study_tips',
            'honeymoons_getaways',
            'horse_racing',
            'horses',
            'hotels',
            'human_resources',
            'humor',
            'hunting_shooting',
            'hybrid',
            'ibs_crohns_disease',
            'illegal_content',
            'immigration',
            'incentivized',
            'incest_abuse_support',
            'incontinence',
            'infertility',
            'inline_skating',
            'insurance',
            'interior_decorating',
            'international_news',
            'internet_technology',
            'investing',
            'investors_and_patents',
            'islam',
            'italian_cuisine',
            'italy',
            'japan',
            'japanese_cuisine',
            'java',
            'javascript',
            'jewelry',
            'jewelry_making',
            'job_fairs',
            'job_search',
            'judaism',
            'k_6_educators',
            'landscaping',
            'language_learning',
            'large_animals',
            'latter_day_saints',
            'law_government_and_politics',
            'legal_issues',
            'local_news',
            'logistics',
            'luxury',
            'mp3_midi',
            'mac_support',
            'magic_and_illusion',
            'marketing',
            'marriage',
            'martial_arts',
            'mens_health',
            'metals',
            'mexican_cuisine',
            'mexico_and_central_america',
            'minivan',
            'mororcycles',
            'mountain_biking',
            'movies',
            'music',
            'mutual_funds',
            'nascar_racing',
            'national_news',
            'national_parks',
            'needlework',
            'net_conferencing',
            'net_for_beginners',
            'hacking',
            'media',
            'non_standard_content',
            'nursing',
            'nutrition',
            'off_road_vehicles',
            'olympics',
            'options',
            'orthopedics',
            'pc_support',
            'pagan_wiccan',
            'paintball',
            'painting',
            'palmtops_pdas',
            'panic_anxiety_disorders',
            'paranormal_phenomena',
            'parenting_k_6_kids',
            'parenting_teens',
            'pediatrics',
            'performance_vehicles',
            'finance',
            'pets',
            'photography',
            'physical_therapy',
            'physics',
            'pickup',
            'politics',
            'portable',
            'power_and_motorcycles',
            'pregnancy',
            'private_school',
            'pro_basketball',
            'pro_ice_hockey',
            'psychology_psychiatry',
            'radio',
            'real_estate',
            'religion',
            'remodeling_and_construction',
            'reptiles',
            'resume_writing_advice',
            'retirement_planning',
            'road_side_assistance',
            'rodeo',
            'roleplaying_games',
            'rugby',
            'running_jogging',
            'sailing',
            'saltwater_fishing',
            'scholarships',
            'sci_fi_and_fantasy',
            'science',
            'scrapbooking',
            'screenwriting',
            'scuba_diving',
            'sedan',
            'senior_living',
            'senor_health',
            'sexuality',
            'shareware_freeware',
            'shopping',
            'skateboarding',
            'skiing',
            'sleep_disorders',
            'smoking_cessation',
            'snowboarding',
            'society',
            'south_america',
            'space_astronomy',
            'spas',
            'special_education',
            'special_needs_kids',
            'sports',
            'stamps_and_coins',
            'stocks',
            'studying_business',
            'style_and_fashion',
            'substance_abuse',
            'surfing_bodyboarding',
            'swimming',
            'table_tennis_ping_pong',
            'tax_planning',
            'tech',
            'teens',
            'telecommuting',
            'television',
            'tennis',
            'theme_parks',
            'thyroid_disease',
            'travel',
            'traveling_with_kids',
            'trucks_and_accessories',
            'us_government_resources',
            'us_military',
            'uncategorized',
            'under_construction',
            'united_kingdom',
            'unix',
            'vegan',
            'vegetarian',
            'veterinary_medicine',
            'video_and_computer_games',
            'vintage_cars',
            'visual_basic',
            'volleyball',
            'wagon',
            'walking',
            'warez',
            'waterski_wakeboard',
            'weather',
            'web_clip_art',
            'web_design_html',
            'search_engine',
            'weddings',
            'weight_loss',
            'windows',
            'wine',
            'womens_health',
            'woodworking',
            'world_soccer'
        ];
    }

}
}());

;(function() {
"use strict";

/* global angular */

angular.module('search')

    .config(['$routeProvider', ModuleConfig]);

function ModuleConfig($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'search.html',
            controller: 'SearchController',
        });

}
}());

;(function() {
"use strict";

/* global angular, firebase */

angular.module('search')
    .controller('SearchController', ['$scope', '$firebaseAuth', '$firebaseObject', '$firebaseArray', 'Filters', SearchController]);

function SearchController($scope, $firebaseAuth, $firebaseObject, $firebaseArray, Filters) {

    var searchRef = firebase.database().ref('searches');

    var searches = $firebaseArray(searchRef);
 
    $scope.searches = searches;

    var searchDebounce = false;
    $scope.$watch('search', saveSearch, true);
    
    $scope.articles = [];

    function saveSearch(newValue, oldValue) {
        // if (!searchDebounce) {
        //     searchDebounce = true;
        //     setTimeout(function() {
        //         searchDebounce = false;
        //     }, 5 * 1000);
        if (!oldValue || newValue !== oldValue) {
            if (!newValue) {
                $scope.articles = [];
            }
            if (!oldValue || newValue.articles !== oldValue.articles) {
                $scope.articles = [];
                var articles = firebase.database().ref('articles');
                angular.forEach(newValue.articles, function(articleId) {
                    var articleRef = $firebaseObject(articles.child(articleId));
                    articleRef.$loaded(function(article) {
                        console.log('article', article);
                        $scope.articles.push(article);
                    }, function(err) {
                        console.log(err);
                    });
                });
            }
            return searches.$save($scope.search);
        }
    }

    $scope.options = Filters;

    $scope.createSearch = function() {
        searches.$add({
            name: 'New search ' + (searches.length + 1),
            //from: 0,
            order: '',
            filters: [],
            size: 100,
            sort: '',
        }).then(function() {
            $scope.search = searches[searches.length - 1];
        });
    };

    $scope.addFilter = function() {
        if (!$scope.search.filters) {
            $scope.search.filters = [];
        }
        var filterOptions = $scope.options.filters[$scope.search.newFilter];
        var filterObject = {
            // firebase doesn't allow undefined values
            name: $scope.search.newFilter,
            link: '',
            equality: '',
            value: filterOptions.value || '',
            type: filterOptions.type || '',
            label: filterOptions.label || '',
            options: filterOptions.options || '',
        };
        $scope.search.filters.push(filterObject);
    };

    // var search = $firebaseObject(searchRef);

    //  search.$bindTo($scope, 'search'); // 3 way binding (no save required)

    //  $scope.search = search;

    // var articles = firebase.database('articles').ref();

    // $scope.articles = $firebaseArray(articles);

}
}());

;(function() {
"use strict";

/* global angular */

angular.module('search').filter('webhose', searchFilter);

function searchFilter() {
    return function(webhoseParameter) {
        if (typeof webhoseParameter === 'string') {
            var filtered = webhoseParameter;
            filtered = filtered.replace(/\./g, ' ');
            filtered = filtered.replace(/_/g, ' ');
            filtered = filtered.replace(/social/i, '');
            filtered = filtered.replace(/persons/g, 'people');
            return filtered.trim();
        }
    };
}
}());

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUuanMiLCJzZWFyY2gvc2VhcmNoLm1vZHVsZS5qcyIsInNlYXJjaC9maWx0ZXJzLmNvbnN0YW50LmpzIiwic2VhcmNoL3NlYXJjaC5jb25maWcuanMiLCJzZWFyY2gvc2VhcmNoLmNvbnRyb2xsZXIuanMiLCJzZWFyY2gvc2VhcmNoLmZpbHRlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUNiQTtBQUNBO0FBQ0E7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQ2prQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUN4RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogZ2xvYmFsIGFuZ3VsYXIsIGZpcmViYXNlICovXG5cbnZhciBjb25maWcgPSB7XG5hcGlLZXk6IFwiQUl6YVN5QkM4OEtBcm1OMjdfaXdZbWltRWxidWd6LTlZXzQwMUpVXCIsXG5hdXRoRG9tYWluOiBcImluYm91bmQtcGxhbmV0LTIxODQyMi5maXJlYmFzZWFwcC5jb21cIixcbmRhdGFiYXNlVVJMOiBcImh0dHBzOi8vaW5ib3VuZC1wbGFuZXQtMjE4NDIyLmZpcmViYXNlaW8uY29tXCIsXG5wcm9qZWN0SWQ6IFwiaW5ib3VuZC1wbGFuZXQtMjE4NDIyXCIsXG5zdG9yYWdlQnVja2V0OiBcImluYm91bmQtcGxhbmV0LTIxODQyMi5hcHBzcG90LmNvbVwiLFxubWVzc2FnaW5nU2VuZGVySWQ6IFwiNDExNjg5NTU1MjBcIlxufTtcblxuZmlyZWJhc2UuaW5pdGlhbGl6ZUFwcChjb25maWcpO1xuXG5hbmd1bGFyLm1vZHVsZSgnYXBwJywgWydmaXJlYmFzZScsJ3RlbXBsYXRlcycsICdzZWFyY2gnXSk7XG4iLCIvKiBnbG9iYWwgYW5ndWxhciAqL1xuXG5hbmd1bGFyLm1vZHVsZSgnc2VhcmNoJywgWyduZ0NzdicsICduZ1JvdXRlJywgJ25nU2FuaXRpemUnXSk7XG4iLCIvKiBnbG9iYWwgYW5ndWxhciAqL1xuXG5hbmd1bGFyLm1vZHVsZSgnc2VhcmNoJylcbiAgICAuY29uc3RhbnQoJ0ZpbHRlcnMnLCAoRmlsdGVycygpKSk7XG5cbmZ1bmN0aW9uIEZpbHRlcnMoKSB7XG5cbiAgICByZXR1cm4ge1xuXG4gICAgICAgIHNvcnQ6IFtcbiAgICAgICAgICAgICdyZWxldmFuY3knLCAnY3Jhd2xlZCcsICdwdWJsaXNoZWQnLCAndGhyZWFkLnB1Ymxpc2hlZCcsICdzb2NpYWwuZmFjZWJvb2subGlrZXMnLCAnc29jaWFsLmZhY2Vib29rLnNoYXJlcycsICdzb2NpYWwuZmFjZWJvb2suY29tbWVudHMnLCAnc29jaWFsLmdwbHVzLnNoYXJlcycsICdzb2NpYWwucGludGVyZXN0LnNoYXJlcycsICdzb2NpYWwubGlua2VkaW4uc2hhcmVzJywgJ3NvY2lhbC5zdHVtYmxlZHVwb24uc2hhcmVzJywgJ3NvY2lhbC52ay5zaGFyZXMnLCAncmVwbGllc19jb3VudCcsICdwYXJ0aWNpcGFudHNfY291bnQnLCAnc3BhbV9zY29yZScsICdwZXJmb3JtYW5jZV9zY29yZScsICdkb21haW5fcmFuaycsICdvcmRfaW5fdGhyZWFkJywgJ3JhdGluZydcbiAgICAgICAgXSwgLy8gc29ydCBvcHRpb25zXG5cbiAgICAgICAgb3JkZXI6IFtcbiAgICAgICAgICAgIHsgbGFiZWw6ICdhc2NlbmRpbmcnLCB2YWx1ZTogJ2FzYycgfSxcbiAgICAgICAgICAgIHsgbGFiZWw6ICdkZXNjZW5kaW5nJywgdmFsdWU6ICdkZXNjJyB9XG4gICAgICAgIF0sIC8vIG9yZGVyIG9wYXRpb25zIChsYWJlbC92YWx1ZSlcblxuICAgICAgICBsaW5rczogW1xuICAgICAgICAgICAgeyBsYWJlbDogJ29yJywgdmFsdWU6ICcgT1IgJyB9LFxuICAgICAgICAgICAgeyBsYWJlbDogJ2FuZCcsIHZhbHVlOiAnIEFORCAnIH1cbiAgICAgICAgXSwgLy8gbGluayBvcHRpb25zIChsYWJlbC92YWx1ZSlcblxuICAgICAgICBlcXVhbGl0eToge1xuICAgICAgICAgICAgb2JqZWN0OiBmYWxzZSxcbiAgICAgICAgICAgIGNoZWNrYm94OiBmYWxzZSxcbiAgICAgICAgICAgIHRleHQ6IFt7IGxhYmVsOiAnaXMnLCB2YWx1ZTogJycgfSwgeyBsYWJlbDogJ2lzIG5vdCcsIHZhbHVlOiAnIScgfV0sXG4gICAgICAgICAgICBudW1iZXI6IFt7IGxhYmVsOiAnZXF1YWwgdG8nLCB2YWx1ZTogJycgfSwgeyBsYWJlbDogJ25vdCBlcXVhbCB0bycsIHZhbHVlOiAnIScgfSwgeyBsYWJlbDogJ2dyZWF0ZXIgdGhhbicsIHZhbHVlOiAnPicgfSwgeyBsYWJlbDogJ2xlc3MgdGhhbicsIHZhbHVlOiAnPCcgfV0sXG4gICAgICAgICAgICBkYXRlOiBbeyBsYWJlbDogJ29uJywgdmFsdWU6ICcnIH0sIHsgbGFiZWw6ICdub3Qgb24nLCB2YWx1ZTogJyEnIH0sIHsgbGFiZWw6ICdhZnRlcicsIHZhbHVlOiAnPicgfSwgeyBsYWJlbDogJ2JlZm9yZScsIHZhbHVlOiAnPCcgfV0sXG4gICAgICAgIH0sIC8vIGVxdWFsaXR5IG9wdGlvbnMgKGxhYmVsL3ZhbHVlKVxuXG4gICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAgIHRleHQ6IHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ2FydGljbGUgdGV4dCcsXG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBlbnRpdHlfc2VudGltZW50OiB7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ3NlbnRpbWVudCcsXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdzZW50aW1lbnQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdvYmplY3QnLFxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XG4gICAgICAgICAgICAgICAgICAgIGVudGl0eVR5cGU6ICcnLFxuICAgICAgICAgICAgICAgICAgICBzZW50aW1lbnQ6ICcnLFxuICAgICAgICAgICAgICAgICAgICBlbnRpdHk6ICcnLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb3B0aW9uczoge1xuICAgICAgICAgICAgICAgICAgICBzZW50aW1lbnQ6IFsncG9zaXRpdmUnLCAnbmV1dHJhbCcsICduZWdhdGl2ZSddLFxuICAgICAgICAgICAgICAgICAgICBlbnRpdHk6IFsncGVyc29uJywgJ29yZ2FuaXphdGlvbiddXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHBlcnNvbjoge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgb3JnYW5pemF0aW9uOiB7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBsb2NhdGlvbjoge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc2l0ZV90eXBlOiB7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ3NlbGVjdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxuICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcbiAgICAgICAgICAgICAgICAgICAgJ25ld3MnLCAnYmxvZ3MnLCAnZGlzY3Vzc2lvbnMnXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHNpdGVfY2F0ZWdvcnk6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnc2VsZWN0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXG4gICAgICAgICAgICAgICAgb3B0aW9uczogY2F0ZWdvcmllcygpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcHVibGlzaGVkOiB7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiAnZGF0ZScsXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdkYXRlIHB1Ymxpc2hlZCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBwZXJmb3JtYW5jZV9zY29yZToge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAndmlyYWxpdHkgc2NvcmUgKDEtMTApJyxcbiAgICAgICAgICAgICAgICBtaW46IDAsXG4gICAgICAgICAgICAgICAgbWF4OiAxMCxcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdudW1iZXInXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZG9tYWluX3Jhbms6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGxhYmVsOiAnZG9tYWluIHJhbmsgKDEtMTAwMCknLFxuICAgICAgICAgICAgICAgIG1pbjogMSxcbiAgICAgICAgICAgICAgICBtYXg6IDEwMDBcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBsYW5ndWFnZToge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICd0aHJlYWQuY291bnRyeSc6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcbiAgICAgICAgICAgICAgICBsYWJlbDogJ3B1YmxpY2F0aW9uIGNvdW50cnknXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGhyZWFkX3RpdGxlOiB7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdwdWJsaWNhdGlvbiB0aXRsZSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhdXRob3I6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHNwYW1fc2NvcmU6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGxhYmVsOiAnc3BhbSBzY29yZSAoMC0xKScsXG4gICAgICAgICAgICAgICAgbWluOiAwLFxuICAgICAgICAgICAgICAgIG1heDogMVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGlzX2ZpcnN0OiB7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiAnY2hlY2tib3gnLFxuICAgICAgICAgICAgICAgIGxhYmVsOiAnaWdub3JlIGNvbW1lbnRzJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRocmVhZF91cmw6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcbiAgICAgICAgICAgICAgICBsYWJlbDogJ3VybCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBleHRlcm5hbF9saW5rczoge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxuICAgICAgICAgICAgICAgIGxhYmVsOiAncmVmZXJlbmNlZCBVUkwnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gJ3NvY2lhbC5mYWNlYm9vay5saWtlcyc6IHtcbiAgICAgICAgICAgIC8vICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgLy8gICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgLy8gICAgIG1pbjogMCxcbiAgICAgICAgICAgIC8vICAgICBtYXg6IDFcbiAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICAvLyAnc29jaWFsLmZhY2Vib29rLnNoYXJlcyc6IHtcbiAgICAgICAgICAgIC8vICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgLy8gICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgLy8gICAgIG1pbjogMCxcbiAgICAgICAgICAgIC8vICAgICBtYXg6IDFcbiAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICAvLyAnc29jaWFsLmZhY2Vib29rLmNvbW1lbnRzJzoge1xuICAgICAgICAgICAgLy8gICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAvLyAgICAgdHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAvLyAgICAgbWluOiAwLFxuICAgICAgICAgICAgLy8gICAgIG1heDogMVxuICAgICAgICAgICAgLy8gfSxcbiAgICAgICAgICAgIC8vICdzb2NpYWwuZ2xwbHVzLnNoYXJlcyc6IHtcbiAgICAgICAgICAgIC8vICAgICBlbGVtZW50OiAnaW5wdXQnLFxuICAgICAgICAgICAgLy8gICAgIHR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgLy8gICAgIG1pbjogMCxcbiAgICAgICAgICAgIC8vICAgICBtYXg6IDFcbiAgICAgICAgICAgIC8vIH0sXG4gICAgICAgICAgICAvLyAnc29jaWFsLnBpbnRlcmVzdC5zaGFyZXMnOiB7XG4gICAgICAgICAgICAvLyAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgIC8vICAgICB0eXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgIC8vICAgICBtaW46IDAsXG4gICAgICAgICAgICAvLyAgICAgbWF4OiAxXG4gICAgICAgICAgICAvLyB9LFxuICAgICAgICAgICAgLy8gJ3NvY2lhbC5saW5rZWRpbi5zaGFyZXMnOiB7XG4gICAgICAgICAgICAvLyAgICAgZWxlbWVudDogJ2lucHV0JyxcbiAgICAgICAgICAgIC8vICAgICB0eXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgIC8vICAgICBtaW46IDAsXG4gICAgICAgICAgICAvLyAgICAgbWF4OiAxXG4gICAgICAgICAgICAvLyB9LFxuICAgICAgICAgICAgLy8gJ3NvY2lhbC5zdHVtYmxlZHVwb24uc2hhcmVzJzoge1xuICAgICAgICAgICAgLy8gICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAvLyAgICAgdHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAvLyAgICAgbWluOiAwLFxuICAgICAgICAgICAgLy8gICAgIG1heDogMVxuICAgICAgICAgICAgLy8gfSxcbiAgICAgICAgICAgIC8vICdzb2NpYWwudmsuc2hhcmVzJzoge1xuICAgICAgICAgICAgLy8gICAgIGVsZW1lbnQ6ICdpbnB1dCcsXG4gICAgICAgICAgICAvLyAgICAgdHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAvLyAgICAgbWluOiAwLFxuICAgICAgICAgICAgLy8gICAgIG1heDogMVxuICAgICAgICAgICAgLy8gfVxuICAgICAgICB9IC8vIGZpbHRlciBvcHRpb25zIChuYW1lOiBvcHRpb25zT2JqZWN0KVxuXG4gICAgfTtcblxuICAgIGZ1bmN0aW9uIGNhdGVnb3JpZXMoKSB7XG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAnM2RfZ3JhcGhpY3MnLFxuICAgICAgICAgICAgJzdfMTJfZWR1Y2F0aW9uJyxcbiAgICAgICAgICAgICdhX2RfZCcsXG4gICAgICAgICAgICAnYWlkc19oaXYnLFxuICAgICAgICAgICAgJ2FjY2Vzc29yaWVzJyxcbiAgICAgICAgICAgICdhZG9wdGlvbicsXG4gICAgICAgICAgICAnYWR1bHRfZWR1Y2F0aW9uJyxcbiAgICAgICAgICAgICdhZHZlbnR1cmVfdHJhdmVsJyxcbiAgICAgICAgICAgICdhZHZlcnRpc2luZycsXG4gICAgICAgICAgICAnYWZyaWNhJyxcbiAgICAgICAgICAgICdhZ3JpY3VsdHVyZScsXG4gICAgICAgICAgICAnYWlyX3RyYXZlbCcsXG4gICAgICAgICAgICAnYWxsZXJnaWVzJyxcbiAgICAgICAgICAgICdhbHRlcm5hdGl2ZV9tZWRpY2luZScsXG4gICAgICAgICAgICAnYWx0ZXJuYXRpdmVfcmVsaWdpb25zJyxcbiAgICAgICAgICAgICdhbWVyaWNhbl9jdWlzaW5lJyxcbiAgICAgICAgICAgICdhbmltYXRpb24nLFxuICAgICAgICAgICAgJ2FudGl2aXJ1c19zb2Z0d2FyZScsXG4gICAgICAgICAgICAnYXBhcnRtZW50cycsXG4gICAgICAgICAgICAnYXBwbGlhbmNlcycsXG4gICAgICAgICAgICAnYXF1YXJpdW1zJyxcbiAgICAgICAgICAgICdhcmNoaXRlY3RzJyxcbiAgICAgICAgICAgICdhcnRfaGlzdG9yeScsXG4gICAgICAgICAgICAnYXJ0X3RlY2hub2xvZ3knLFxuICAgICAgICAgICAgJ2FydGhyaXRpcycsXG4gICAgICAgICAgICAnYXJ0c19hbmRfY3JhZnRzJyxcbiAgICAgICAgICAgICdhc3RobWEnLFxuICAgICAgICAgICAgJ2FzdHJvbG9neScsXG4gICAgICAgICAgICAnYXRoZWlzbV9hZ25vc3RpY2lzbScsXG4gICAgICAgICAgICAnYXVzdHJhbGlhX2FuZF9uZXdfemVhbGFuZCcsXG4gICAgICAgICAgICAnYXV0aXNtX3BkZCcsXG4gICAgICAgICAgICAnYXV0b19wYXJ0cycsXG4gICAgICAgICAgICAnYXV0b19yYWNpbmcnLFxuICAgICAgICAgICAgJ2F1dG9fcmVwYWlyJyxcbiAgICAgICAgICAgICd2ZWhpY2xlcycsXG4gICAgICAgICAgICAnYmFiaWVzX2FuZF90b2RkbGVycycsXG4gICAgICAgICAgICAnYmFyYmVjdWVzX2FuZF9ncmlsbGluZycsXG4gICAgICAgICAgICAnYmFzZWJhbGwnLFxuICAgICAgICAgICAgJ2JlYWR3b3JrJyxcbiAgICAgICAgICAgICdiZWF1dHknLFxuICAgICAgICAgICAgJ2JlZF9hbmRfYnJlYWtmYXN0cycsXG4gICAgICAgICAgICAnYmVnaW5uaW5nX2ludmVzdGluZycsXG4gICAgICAgICAgICAnYmljeWNsaW5nJyxcbiAgICAgICAgICAgICdiaW9sb2d5JyxcbiAgICAgICAgICAgICdiaW90ZWNoX2Jpb21lZGljYWwnLFxuICAgICAgICAgICAgJ2JpcG9sYXJfZGlzb3JkZXInLFxuICAgICAgICAgICAgJ2JpcmRzJyxcbiAgICAgICAgICAgICdiaXJkd2F0Y2hpbmcnLFxuICAgICAgICAgICAgJ2dhbWVzJyxcbiAgICAgICAgICAgICdib2R5X2FydCcsXG4gICAgICAgICAgICAnYm9keWJ1aWxkaW5nJyxcbiAgICAgICAgICAgICdib29rc19hbmRfbGl0ZXJhdHVyZScsXG4gICAgICAgICAgICAnYm90YW55JyxcbiAgICAgICAgICAgICdib3hpbmcnLFxuICAgICAgICAgICAgJ2JyYWluX3R1bW9yJyxcbiAgICAgICAgICAgICdidWRkaGlzbScsXG4gICAgICAgICAgICAnYnVkZ2V0X3RyYXZlbCcsXG4gICAgICAgICAgICAnYnVzaW5lc3MnLFxuICAgICAgICAgICAgJ2J1c2luZXNzX3NvZnR3YXJlJyxcbiAgICAgICAgICAgICdidXNpbmVzc190cmF2ZWwnLFxuICAgICAgICAgICAgJ2J1eWluZ19zZWxsaW5nX2NhcnMnLFxuICAgICAgICAgICAgJ2J1eWluZ19zZWxsaW5nX2hvbWVzJyxcbiAgICAgICAgICAgICdieV91c19sb2NhbGUnLFxuICAgICAgICAgICAgJ2NfY19wbHVzX3BsdXMnLFxuICAgICAgICAgICAgJ2NhanVuX2NyZW9sZScsXG4gICAgICAgICAgICAnY2FtZXJhc19hbmRfY2FtY29yZGVycycsXG4gICAgICAgICAgICAnY2FtcGluZycsXG4gICAgICAgICAgICAnY2FuYWRhJyxcbiAgICAgICAgICAgICdjYW5jZXInLFxuICAgICAgICAgICAgJ2NhbmRsZV9hbmRfc29hcF9tYWtpbmcnLFxuICAgICAgICAgICAgJ2Nhbm9laW5nX2theWFraW5nJyxcbiAgICAgICAgICAgICdjYXJfY3VsdHVyZScsXG4gICAgICAgICAgICAnZ2FtYmxpbmcnLFxuICAgICAgICAgICAgJ2NhcmVlcl9hZHZpY2UnLFxuICAgICAgICAgICAgJ2NhcmVlcl9wbGFubmluZycsXG4gICAgICAgICAgICAnam9icycsXG4gICAgICAgICAgICAnY2FyaWJiZWFuJyxcbiAgICAgICAgICAgICdjYXRob2xpY2lzbScsXG4gICAgICAgICAgICAnY2F0cycsXG4gICAgICAgICAgICAnY2VsZWJyaXR5X2Zhbl9nb3NzaXAnLFxuICAgICAgICAgICAgJ2NlbGxfcGhvbmVzJyxcbiAgICAgICAgICAgICdjZXJ0aWZpZWRfcHJlX293bmVkJyxcbiAgICAgICAgICAgICdjaGVlcmxlYWRpbmcnLFxuICAgICAgICAgICAgJ2NoZW1pc3RyeScsXG4gICAgICAgICAgICAnY2hlc3MnLFxuICAgICAgICAgICAgJ2NoaW5lc2VfY3Vpc2luZScsXG4gICAgICAgICAgICAnY2hvbGVzdGVyb2wnLFxuICAgICAgICAgICAgJ2NocmlzdGlhbml0eScsXG4gICAgICAgICAgICAnY2hyb25pY19mYXRpZ3VlX3N5bmRyb21lJyxcbiAgICAgICAgICAgICdjaHJvbmljX3BhaW4nLFxuICAgICAgICAgICAgJ2NpZ2FycycsXG4gICAgICAgICAgICAnY2xpbWJpbmcnLFxuICAgICAgICAgICAgJ2Nsb3RoaW5nJyxcbiAgICAgICAgICAgICdjb2NrdGFpbHNfYmVlcicsXG4gICAgICAgICAgICAnY29mZmVlX3RlYScsXG4gICAgICAgICAgICAnY29sZF9hbmRfZmx1JyxcbiAgICAgICAgICAgICdjb2xsZWN0aW5nJyxcbiAgICAgICAgICAgICdjb2xsZWdlJyxcbiAgICAgICAgICAgICdjb2xsZWdlX2FkbWluaXN0cmF0aW9uJyxcbiAgICAgICAgICAgICdjb2xsZWdlX2xpZmUnLFxuICAgICAgICAgICAgJ2NvbWljX2Jvb2tzJyxcbiAgICAgICAgICAgICdjb21tZW50YXJ5JyxcbiAgICAgICAgICAgICdjb21wYXJpc29uJyxcbiAgICAgICAgICAgICdjb21wdXRlcl9jZXJ0aWZpY2F0aW9uJyxcbiAgICAgICAgICAgICdjb21wdXRlcl9uZXR3b3JraW5nJyxcbiAgICAgICAgICAgICdjb21wdXRlcl9wZXJpcGhlcmFscycsXG4gICAgICAgICAgICAnY29tcHV0ZXJfcmV2aWV3cycsXG4gICAgICAgICAgICAnY29uc3RydWN0aW9uJyxcbiAgICAgICAgICAgICdjb250ZXN0c19hbmRfZnJlZWJpZXMnLFxuICAgICAgICAgICAgJ2NvbnZlcnRpYmxlJyxcbiAgICAgICAgICAgICdjb3B5cmlnaHRfaW5mcmluZ2VtZW50JyxcbiAgICAgICAgICAgICdjb3VwZScsXG4gICAgICAgICAgICAnY291cG9uaW5nJyxcbiAgICAgICAgICAgICdjcmVkaXRfZGVidF9hbmRfbG9hbnMnLFxuICAgICAgICAgICAgJ2NyaWNrZXQnLFxuICAgICAgICAgICAgJ2Nyb3Nzb3ZlcicsXG4gICAgICAgICAgICAnY3J1aXNlcycsXG4gICAgICAgICAgICAnY3Vpc2luZV9zcGVjaWZpYycsXG4gICAgICAgICAgICAnZGF0YV9jZW50ZXJzJyxcbiAgICAgICAgICAgICdkYXRhYmFzZXMnLFxuICAgICAgICAgICAgJ2RhdGluZycsXG4gICAgICAgICAgICAnZGF5Y2FyZV9wcmVfc2Nob29sJyxcbiAgICAgICAgICAgICdkZWFmbmVzcycsXG4gICAgICAgICAgICAnZGVudGFsX2NhcmUnLFxuICAgICAgICAgICAgJ2RlcHJlc3Npb24nLFxuICAgICAgICAgICAgJ2Rlcm1hdG9sb2d5JyxcbiAgICAgICAgICAgICdkZXNrdG9wX3B1Ymxpc2hpbmcnLFxuICAgICAgICAgICAgJ2Rlc2t0b3BfdmlkZW8nLFxuICAgICAgICAgICAgJ2Rlc3NlcnRzX2FuZF9iYWtpbmcnLFxuICAgICAgICAgICAgJ2RpYWJldGVzJyxcbiAgICAgICAgICAgICdkaWVzZWwnLFxuICAgICAgICAgICAgJ2RpbmluZ19vdXQnLFxuICAgICAgICAgICAgJ2Rpc3RhbmNlX2xlYXJuaW5nJyxcbiAgICAgICAgICAgICdkaXZvcmNlX3N1cHBvcnQnLFxuICAgICAgICAgICAgJ2RvZ3MnLFxuICAgICAgICAgICAgJ2RyYXdpbmdfc2tldGNoaW5nJyxcbiAgICAgICAgICAgICdlYXN0ZXJuX2V1cm9wZScsXG4gICAgICAgICAgICAnZWR1Y2F0aW9uJyxcbiAgICAgICAgICAgICdlbGRlcmNhcmUnLFxuICAgICAgICAgICAgJ2VsZWN0cmljX3ZlaGljbGUnLFxuICAgICAgICAgICAgJ2VtYWlsJyxcbiAgICAgICAgICAgICdlbmdpbmVzJyxcbiAgICAgICAgICAgICdlbmdsaXNoX2FzX2FfMm5kX2xhbmd1YWdlJyxcbiAgICAgICAgICAgICdlbnRlcnRhaW5pbmcnLFxuICAgICAgICAgICAgJ2VudGVydGFpbm1lbnQnLFxuICAgICAgICAgICAgJ2Vudmlyb25tZW50YWxfc2FmZXR5JyxcbiAgICAgICAgICAgICdlcGlsZXBzeScsXG4gICAgICAgICAgICAnZXRobmljX3NwZWNpZmljJyxcbiAgICAgICAgICAgICdldXJvcGUnLFxuICAgICAgICAgICAgJ2V4ZXJjaXNlJyxcbiAgICAgICAgICAgICdhZHVsdCcsXG4gICAgICAgICAgICAnZmFtaWx5X2FuZF9wYXJlbnRpbmcnLFxuICAgICAgICAgICAgJ2ZhbWlseV9pbnRlcm5ldCcsXG4gICAgICAgICAgICAnZmFzaGlvbicsXG4gICAgICAgICAgICAnZmlndXJlX3NrYXRpbmcnLFxuICAgICAgICAgICAgJ2ZpbmFuY2lhbF9haWQnLFxuICAgICAgICAgICAgJ2ZpbmFuY2lhbF9uZXdzJyxcbiAgICAgICAgICAgICdmaW5hbmNpYWxfcGxhbm5pbmcnLFxuICAgICAgICAgICAgJ2ZpbmVfYXJ0JyxcbiAgICAgICAgICAgICdmbHlfZmlzaGluZycsXG4gICAgICAgICAgICAnZm9vZCcsXG4gICAgICAgICAgICAnZm9vZF9hbGxlcmdpZXMnLFxuICAgICAgICAgICAgJ2Zvb3RiYWxsJyxcbiAgICAgICAgICAgICdmb3Jlc3RyeScsXG4gICAgICAgICAgICAnZnJhbmNlJyxcbiAgICAgICAgICAgICdmcmVlbGFuY2Vfd3JpdGluZycsXG4gICAgICAgICAgICAnZnJlbmNoX2N1aXNpbmUnLFxuICAgICAgICAgICAgJ2ZyZXNod2F0ZXJfZmlzaGluZycsXG4gICAgICAgICAgICAnZ2VyZF9hY2lkX3JlZmx1eCcsXG4gICAgICAgICAgICAnZ2FtZV9hbmRfZmlzaCcsXG4gICAgICAgICAgICAnZ2FyZGVuaW5nJyxcbiAgICAgICAgICAgICdnYXlfbGlmZScsXG4gICAgICAgICAgICAnZ2VuZWFsb2d5JyxcbiAgICAgICAgICAgICdnZW9ncmFwaHknLFxuICAgICAgICAgICAgJ2dlb2xvZ3knLFxuICAgICAgICAgICAgJ2dldHRpbmdfcHVibGlzaGVkJyxcbiAgICAgICAgICAgICdnb2xmJyxcbiAgICAgICAgICAgICdnb3Zlcm5tZW50JyxcbiAgICAgICAgICAgICdncmFkdWF0ZV9zY2hvb2wnLFxuICAgICAgICAgICAgJ2dyYXBoaWNzX3NvZnR3YXJlJyxcbiAgICAgICAgICAgICdncmVlY2UnLFxuICAgICAgICAgICAgJ2dyZWVuX3NvbHV0aW9ucycsXG4gICAgICAgICAgICAnZ3VpdGFyJyxcbiAgICAgICAgICAgICdoYXRjaGJhY2snLFxuICAgICAgICAgICAgJ2hhdGVfY29udGVudCcsXG4gICAgICAgICAgICAnaGVhZGFjaGVzX21pZ3JhaW5lcycsXG4gICAgICAgICAgICAnaGVhbHRoJyxcbiAgICAgICAgICAgICdoZWFsdGhfbG93ZmF0X2Nvb2tpbmcnLFxuICAgICAgICAgICAgJ2hlYXJ0X2Rpc2Vhc2UnLFxuICAgICAgICAgICAgJ2hlZGdlX2Z1bmQnLFxuICAgICAgICAgICAgJ2hlcmJzX2Zvcl9oZWFsdGgnLFxuICAgICAgICAgICAgJ2hpbmR1aXNtJyxcbiAgICAgICAgICAgICdob2JiaWVzX2FuZF9pbnRlcmVzdHMnLFxuICAgICAgICAgICAgJ2hvbGlzdGljX2hlYWxpbmcnLFxuICAgICAgICAgICAgJ2hvbWVfYW5kX2dhcmRlbicsXG4gICAgICAgICAgICAnaG9tZV9yZWNvcmRpbmcnLFxuICAgICAgICAgICAgJ2hvbWVfcmVwYWlyJyxcbiAgICAgICAgICAgICdob21lX3RoZWF0ZXInLFxuICAgICAgICAgICAgJ2hvbWVfdmlkZW9fZHZkJyxcbiAgICAgICAgICAgICdob21lc2Nob29saW5nJyxcbiAgICAgICAgICAgICdob21ld29ya19zdHVkeV90aXBzJyxcbiAgICAgICAgICAgICdob25leW1vb25zX2dldGF3YXlzJyxcbiAgICAgICAgICAgICdob3JzZV9yYWNpbmcnLFxuICAgICAgICAgICAgJ2hvcnNlcycsXG4gICAgICAgICAgICAnaG90ZWxzJyxcbiAgICAgICAgICAgICdodW1hbl9yZXNvdXJjZXMnLFxuICAgICAgICAgICAgJ2h1bW9yJyxcbiAgICAgICAgICAgICdodW50aW5nX3Nob290aW5nJyxcbiAgICAgICAgICAgICdoeWJyaWQnLFxuICAgICAgICAgICAgJ2lic19jcm9obnNfZGlzZWFzZScsXG4gICAgICAgICAgICAnaWxsZWdhbF9jb250ZW50JyxcbiAgICAgICAgICAgICdpbW1pZ3JhdGlvbicsXG4gICAgICAgICAgICAnaW5jZW50aXZpemVkJyxcbiAgICAgICAgICAgICdpbmNlc3RfYWJ1c2Vfc3VwcG9ydCcsXG4gICAgICAgICAgICAnaW5jb250aW5lbmNlJyxcbiAgICAgICAgICAgICdpbmZlcnRpbGl0eScsXG4gICAgICAgICAgICAnaW5saW5lX3NrYXRpbmcnLFxuICAgICAgICAgICAgJ2luc3VyYW5jZScsXG4gICAgICAgICAgICAnaW50ZXJpb3JfZGVjb3JhdGluZycsXG4gICAgICAgICAgICAnaW50ZXJuYXRpb25hbF9uZXdzJyxcbiAgICAgICAgICAgICdpbnRlcm5ldF90ZWNobm9sb2d5JyxcbiAgICAgICAgICAgICdpbnZlc3RpbmcnLFxuICAgICAgICAgICAgJ2ludmVzdG9yc19hbmRfcGF0ZW50cycsXG4gICAgICAgICAgICAnaXNsYW0nLFxuICAgICAgICAgICAgJ2l0YWxpYW5fY3Vpc2luZScsXG4gICAgICAgICAgICAnaXRhbHknLFxuICAgICAgICAgICAgJ2phcGFuJyxcbiAgICAgICAgICAgICdqYXBhbmVzZV9jdWlzaW5lJyxcbiAgICAgICAgICAgICdqYXZhJyxcbiAgICAgICAgICAgICdqYXZhc2NyaXB0JyxcbiAgICAgICAgICAgICdqZXdlbHJ5JyxcbiAgICAgICAgICAgICdqZXdlbHJ5X21ha2luZycsXG4gICAgICAgICAgICAnam9iX2ZhaXJzJyxcbiAgICAgICAgICAgICdqb2Jfc2VhcmNoJyxcbiAgICAgICAgICAgICdqdWRhaXNtJyxcbiAgICAgICAgICAgICdrXzZfZWR1Y2F0b3JzJyxcbiAgICAgICAgICAgICdsYW5kc2NhcGluZycsXG4gICAgICAgICAgICAnbGFuZ3VhZ2VfbGVhcm5pbmcnLFxuICAgICAgICAgICAgJ2xhcmdlX2FuaW1hbHMnLFxuICAgICAgICAgICAgJ2xhdHRlcl9kYXlfc2FpbnRzJyxcbiAgICAgICAgICAgICdsYXdfZ292ZXJubWVudF9hbmRfcG9saXRpY3MnLFxuICAgICAgICAgICAgJ2xlZ2FsX2lzc3VlcycsXG4gICAgICAgICAgICAnbG9jYWxfbmV3cycsXG4gICAgICAgICAgICAnbG9naXN0aWNzJyxcbiAgICAgICAgICAgICdsdXh1cnknLFxuICAgICAgICAgICAgJ21wM19taWRpJyxcbiAgICAgICAgICAgICdtYWNfc3VwcG9ydCcsXG4gICAgICAgICAgICAnbWFnaWNfYW5kX2lsbHVzaW9uJyxcbiAgICAgICAgICAgICdtYXJrZXRpbmcnLFxuICAgICAgICAgICAgJ21hcnJpYWdlJyxcbiAgICAgICAgICAgICdtYXJ0aWFsX2FydHMnLFxuICAgICAgICAgICAgJ21lbnNfaGVhbHRoJyxcbiAgICAgICAgICAgICdtZXRhbHMnLFxuICAgICAgICAgICAgJ21leGljYW5fY3Vpc2luZScsXG4gICAgICAgICAgICAnbWV4aWNvX2FuZF9jZW50cmFsX2FtZXJpY2EnLFxuICAgICAgICAgICAgJ21pbml2YW4nLFxuICAgICAgICAgICAgJ21vcm9yY3ljbGVzJyxcbiAgICAgICAgICAgICdtb3VudGFpbl9iaWtpbmcnLFxuICAgICAgICAgICAgJ21vdmllcycsXG4gICAgICAgICAgICAnbXVzaWMnLFxuICAgICAgICAgICAgJ211dHVhbF9mdW5kcycsXG4gICAgICAgICAgICAnbmFzY2FyX3JhY2luZycsXG4gICAgICAgICAgICAnbmF0aW9uYWxfbmV3cycsXG4gICAgICAgICAgICAnbmF0aW9uYWxfcGFya3MnLFxuICAgICAgICAgICAgJ25lZWRsZXdvcmsnLFxuICAgICAgICAgICAgJ25ldF9jb25mZXJlbmNpbmcnLFxuICAgICAgICAgICAgJ25ldF9mb3JfYmVnaW5uZXJzJyxcbiAgICAgICAgICAgICdoYWNraW5nJyxcbiAgICAgICAgICAgICdtZWRpYScsXG4gICAgICAgICAgICAnbm9uX3N0YW5kYXJkX2NvbnRlbnQnLFxuICAgICAgICAgICAgJ251cnNpbmcnLFxuICAgICAgICAgICAgJ251dHJpdGlvbicsXG4gICAgICAgICAgICAnb2ZmX3JvYWRfdmVoaWNsZXMnLFxuICAgICAgICAgICAgJ29seW1waWNzJyxcbiAgICAgICAgICAgICdvcHRpb25zJyxcbiAgICAgICAgICAgICdvcnRob3BlZGljcycsXG4gICAgICAgICAgICAncGNfc3VwcG9ydCcsXG4gICAgICAgICAgICAncGFnYW5fd2ljY2FuJyxcbiAgICAgICAgICAgICdwYWludGJhbGwnLFxuICAgICAgICAgICAgJ3BhaW50aW5nJyxcbiAgICAgICAgICAgICdwYWxtdG9wc19wZGFzJyxcbiAgICAgICAgICAgICdwYW5pY19hbnhpZXR5X2Rpc29yZGVycycsXG4gICAgICAgICAgICAncGFyYW5vcm1hbF9waGVub21lbmEnLFxuICAgICAgICAgICAgJ3BhcmVudGluZ19rXzZfa2lkcycsXG4gICAgICAgICAgICAncGFyZW50aW5nX3RlZW5zJyxcbiAgICAgICAgICAgICdwZWRpYXRyaWNzJyxcbiAgICAgICAgICAgICdwZXJmb3JtYW5jZV92ZWhpY2xlcycsXG4gICAgICAgICAgICAnZmluYW5jZScsXG4gICAgICAgICAgICAncGV0cycsXG4gICAgICAgICAgICAncGhvdG9ncmFwaHknLFxuICAgICAgICAgICAgJ3BoeXNpY2FsX3RoZXJhcHknLFxuICAgICAgICAgICAgJ3BoeXNpY3MnLFxuICAgICAgICAgICAgJ3BpY2t1cCcsXG4gICAgICAgICAgICAncG9saXRpY3MnLFxuICAgICAgICAgICAgJ3BvcnRhYmxlJyxcbiAgICAgICAgICAgICdwb3dlcl9hbmRfbW90b3JjeWNsZXMnLFxuICAgICAgICAgICAgJ3ByZWduYW5jeScsXG4gICAgICAgICAgICAncHJpdmF0ZV9zY2hvb2wnLFxuICAgICAgICAgICAgJ3Byb19iYXNrZXRiYWxsJyxcbiAgICAgICAgICAgICdwcm9faWNlX2hvY2tleScsXG4gICAgICAgICAgICAncHN5Y2hvbG9neV9wc3ljaGlhdHJ5JyxcbiAgICAgICAgICAgICdyYWRpbycsXG4gICAgICAgICAgICAncmVhbF9lc3RhdGUnLFxuICAgICAgICAgICAgJ3JlbGlnaW9uJyxcbiAgICAgICAgICAgICdyZW1vZGVsaW5nX2FuZF9jb25zdHJ1Y3Rpb24nLFxuICAgICAgICAgICAgJ3JlcHRpbGVzJyxcbiAgICAgICAgICAgICdyZXN1bWVfd3JpdGluZ19hZHZpY2UnLFxuICAgICAgICAgICAgJ3JldGlyZW1lbnRfcGxhbm5pbmcnLFxuICAgICAgICAgICAgJ3JvYWRfc2lkZV9hc3Npc3RhbmNlJyxcbiAgICAgICAgICAgICdyb2RlbycsXG4gICAgICAgICAgICAncm9sZXBsYXlpbmdfZ2FtZXMnLFxuICAgICAgICAgICAgJ3J1Z2J5JyxcbiAgICAgICAgICAgICdydW5uaW5nX2pvZ2dpbmcnLFxuICAgICAgICAgICAgJ3NhaWxpbmcnLFxuICAgICAgICAgICAgJ3NhbHR3YXRlcl9maXNoaW5nJyxcbiAgICAgICAgICAgICdzY2hvbGFyc2hpcHMnLFxuICAgICAgICAgICAgJ3NjaV9maV9hbmRfZmFudGFzeScsXG4gICAgICAgICAgICAnc2NpZW5jZScsXG4gICAgICAgICAgICAnc2NyYXBib29raW5nJyxcbiAgICAgICAgICAgICdzY3JlZW53cml0aW5nJyxcbiAgICAgICAgICAgICdzY3ViYV9kaXZpbmcnLFxuICAgICAgICAgICAgJ3NlZGFuJyxcbiAgICAgICAgICAgICdzZW5pb3JfbGl2aW5nJyxcbiAgICAgICAgICAgICdzZW5vcl9oZWFsdGgnLFxuICAgICAgICAgICAgJ3NleHVhbGl0eScsXG4gICAgICAgICAgICAnc2hhcmV3YXJlX2ZyZWV3YXJlJyxcbiAgICAgICAgICAgICdzaG9wcGluZycsXG4gICAgICAgICAgICAnc2thdGVib2FyZGluZycsXG4gICAgICAgICAgICAnc2tpaW5nJyxcbiAgICAgICAgICAgICdzbGVlcF9kaXNvcmRlcnMnLFxuICAgICAgICAgICAgJ3Ntb2tpbmdfY2Vzc2F0aW9uJyxcbiAgICAgICAgICAgICdzbm93Ym9hcmRpbmcnLFxuICAgICAgICAgICAgJ3NvY2lldHknLFxuICAgICAgICAgICAgJ3NvdXRoX2FtZXJpY2EnLFxuICAgICAgICAgICAgJ3NwYWNlX2FzdHJvbm9teScsXG4gICAgICAgICAgICAnc3BhcycsXG4gICAgICAgICAgICAnc3BlY2lhbF9lZHVjYXRpb24nLFxuICAgICAgICAgICAgJ3NwZWNpYWxfbmVlZHNfa2lkcycsXG4gICAgICAgICAgICAnc3BvcnRzJyxcbiAgICAgICAgICAgICdzdGFtcHNfYW5kX2NvaW5zJyxcbiAgICAgICAgICAgICdzdG9ja3MnLFxuICAgICAgICAgICAgJ3N0dWR5aW5nX2J1c2luZXNzJyxcbiAgICAgICAgICAgICdzdHlsZV9hbmRfZmFzaGlvbicsXG4gICAgICAgICAgICAnc3Vic3RhbmNlX2FidXNlJyxcbiAgICAgICAgICAgICdzdXJmaW5nX2JvZHlib2FyZGluZycsXG4gICAgICAgICAgICAnc3dpbW1pbmcnLFxuICAgICAgICAgICAgJ3RhYmxlX3Rlbm5pc19waW5nX3BvbmcnLFxuICAgICAgICAgICAgJ3RheF9wbGFubmluZycsXG4gICAgICAgICAgICAndGVjaCcsXG4gICAgICAgICAgICAndGVlbnMnLFxuICAgICAgICAgICAgJ3RlbGVjb21tdXRpbmcnLFxuICAgICAgICAgICAgJ3RlbGV2aXNpb24nLFxuICAgICAgICAgICAgJ3Rlbm5pcycsXG4gICAgICAgICAgICAndGhlbWVfcGFya3MnLFxuICAgICAgICAgICAgJ3RoeXJvaWRfZGlzZWFzZScsXG4gICAgICAgICAgICAndHJhdmVsJyxcbiAgICAgICAgICAgICd0cmF2ZWxpbmdfd2l0aF9raWRzJyxcbiAgICAgICAgICAgICd0cnVja3NfYW5kX2FjY2Vzc29yaWVzJyxcbiAgICAgICAgICAgICd1c19nb3Zlcm5tZW50X3Jlc291cmNlcycsXG4gICAgICAgICAgICAndXNfbWlsaXRhcnknLFxuICAgICAgICAgICAgJ3VuY2F0ZWdvcml6ZWQnLFxuICAgICAgICAgICAgJ3VuZGVyX2NvbnN0cnVjdGlvbicsXG4gICAgICAgICAgICAndW5pdGVkX2tpbmdkb20nLFxuICAgICAgICAgICAgJ3VuaXgnLFxuICAgICAgICAgICAgJ3ZlZ2FuJyxcbiAgICAgICAgICAgICd2ZWdldGFyaWFuJyxcbiAgICAgICAgICAgICd2ZXRlcmluYXJ5X21lZGljaW5lJyxcbiAgICAgICAgICAgICd2aWRlb19hbmRfY29tcHV0ZXJfZ2FtZXMnLFxuICAgICAgICAgICAgJ3ZpbnRhZ2VfY2FycycsXG4gICAgICAgICAgICAndmlzdWFsX2Jhc2ljJyxcbiAgICAgICAgICAgICd2b2xsZXliYWxsJyxcbiAgICAgICAgICAgICd3YWdvbicsXG4gICAgICAgICAgICAnd2Fsa2luZycsXG4gICAgICAgICAgICAnd2FyZXonLFxuICAgICAgICAgICAgJ3dhdGVyc2tpX3dha2Vib2FyZCcsXG4gICAgICAgICAgICAnd2VhdGhlcicsXG4gICAgICAgICAgICAnd2ViX2NsaXBfYXJ0JyxcbiAgICAgICAgICAgICd3ZWJfZGVzaWduX2h0bWwnLFxuICAgICAgICAgICAgJ3NlYXJjaF9lbmdpbmUnLFxuICAgICAgICAgICAgJ3dlZGRpbmdzJyxcbiAgICAgICAgICAgICd3ZWlnaHRfbG9zcycsXG4gICAgICAgICAgICAnd2luZG93cycsXG4gICAgICAgICAgICAnd2luZScsXG4gICAgICAgICAgICAnd29tZW5zX2hlYWx0aCcsXG4gICAgICAgICAgICAnd29vZHdvcmtpbmcnLFxuICAgICAgICAgICAgJ3dvcmxkX3NvY2NlcidcbiAgICAgICAgXTtcbiAgICB9XG5cbn1cbiIsIi8qIGdsb2JhbCBhbmd1bGFyICovXG5cbmFuZ3VsYXIubW9kdWxlKCdzZWFyY2gnKVxuXG4gICAgLmNvbmZpZyhbJyRyb3V0ZVByb3ZpZGVyJywgTW9kdWxlQ29uZmlnXSk7XG5cbmZ1bmN0aW9uIE1vZHVsZUNvbmZpZygkcm91dGVQcm92aWRlcikge1xuXG4gICAgJHJvdXRlUHJvdmlkZXJcbiAgICAgICAgLndoZW4oJy8nLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3NlYXJjaC5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdTZWFyY2hDb250cm9sbGVyJyxcbiAgICAgICAgfSk7XG5cbn1cbiIsIi8qIGdsb2JhbCBhbmd1bGFyLCBmaXJlYmFzZSAqL1xuXG5hbmd1bGFyLm1vZHVsZSgnc2VhcmNoJylcbiAgICAuY29udHJvbGxlcignU2VhcmNoQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRmaXJlYmFzZUF1dGgnLCAnJGZpcmViYXNlT2JqZWN0JywgJyRmaXJlYmFzZUFycmF5JywgJ0ZpbHRlcnMnLCBTZWFyY2hDb250cm9sbGVyXSk7XG5cbmZ1bmN0aW9uIFNlYXJjaENvbnRyb2xsZXIoJHNjb3BlLCAkZmlyZWJhc2VBdXRoLCAkZmlyZWJhc2VPYmplY3QsICRmaXJlYmFzZUFycmF5LCBGaWx0ZXJzKSB7XG5cbiAgICB2YXIgc2VhcmNoUmVmID0gZmlyZWJhc2UuZGF0YWJhc2UoKS5yZWYoJ3NlYXJjaGVzJyk7XG5cbiAgICB2YXIgc2VhcmNoZXMgPSAkZmlyZWJhc2VBcnJheShzZWFyY2hSZWYpO1xuIFxuICAgICRzY29wZS5zZWFyY2hlcyA9IHNlYXJjaGVzO1xuXG4gICAgdmFyIHNlYXJjaERlYm91bmNlID0gZmFsc2U7XG4gICAgJHNjb3BlLiR3YXRjaCgnc2VhcmNoJywgc2F2ZVNlYXJjaCwgdHJ1ZSk7XG4gICAgXG4gICAgJHNjb3BlLmFydGljbGVzID0gW107XG5cbiAgICBmdW5jdGlvbiBzYXZlU2VhcmNoKG5ld1ZhbHVlLCBvbGRWYWx1ZSkge1xuICAgICAgICAvLyBpZiAoIXNlYXJjaERlYm91bmNlKSB7XG4gICAgICAgIC8vICAgICBzZWFyY2hEZWJvdW5jZSA9IHRydWU7XG4gICAgICAgIC8vICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyAgICAgICAgIHNlYXJjaERlYm91bmNlID0gZmFsc2U7XG4gICAgICAgIC8vICAgICB9LCA1ICogMTAwMCk7XG4gICAgICAgIGlmICghb2xkVmFsdWUgfHwgbmV3VmFsdWUgIT09IG9sZFZhbHVlKSB7XG4gICAgICAgICAgICBpZiAoIW5ld1ZhbHVlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFydGljbGVzID0gW107XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoIW9sZFZhbHVlIHx8IG5ld1ZhbHVlLmFydGljbGVzICE9PSBvbGRWYWx1ZS5hcnRpY2xlcykge1xuICAgICAgICAgICAgICAgICRzY29wZS5hcnRpY2xlcyA9IFtdO1xuICAgICAgICAgICAgICAgIHZhciBhcnRpY2xlcyA9IGZpcmViYXNlLmRhdGFiYXNlKCkucmVmKCdhcnRpY2xlcycpO1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuZm9yRWFjaChuZXdWYWx1ZS5hcnRpY2xlcywgZnVuY3Rpb24oYXJ0aWNsZUlkKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBhcnRpY2xlUmVmID0gJGZpcmViYXNlT2JqZWN0KGFydGljbGVzLmNoaWxkKGFydGljbGVJZCkpO1xuICAgICAgICAgICAgICAgICAgICBhcnRpY2xlUmVmLiRsb2FkZWQoZnVuY3Rpb24oYXJ0aWNsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2FydGljbGUnLCBhcnRpY2xlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hcnRpY2xlcy5wdXNoKGFydGljbGUpO1xuICAgICAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbihlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHNlYXJjaGVzLiRzYXZlKCRzY29wZS5zZWFyY2gpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgJHNjb3BlLm9wdGlvbnMgPSBGaWx0ZXJzO1xuXG4gICAgJHNjb3BlLmNyZWF0ZVNlYXJjaCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBzZWFyY2hlcy4kYWRkKHtcbiAgICAgICAgICAgIG5hbWU6ICdOZXcgc2VhcmNoICcgKyAoc2VhcmNoZXMubGVuZ3RoICsgMSksXG4gICAgICAgICAgICAvL2Zyb206IDAsXG4gICAgICAgICAgICBvcmRlcjogJycsXG4gICAgICAgICAgICBmaWx0ZXJzOiBbXSxcbiAgICAgICAgICAgIHNpemU6IDEwMCxcbiAgICAgICAgICAgIHNvcnQ6ICcnLFxuICAgICAgICB9KS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHNjb3BlLnNlYXJjaCA9IHNlYXJjaGVzW3NlYXJjaGVzLmxlbmd0aCAtIDFdO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgJHNjb3BlLmFkZEZpbHRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoISRzY29wZS5zZWFyY2guZmlsdGVycykge1xuICAgICAgICAgICAgJHNjb3BlLnNlYXJjaC5maWx0ZXJzID0gW107XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGZpbHRlck9wdGlvbnMgPSAkc2NvcGUub3B0aW9ucy5maWx0ZXJzWyRzY29wZS5zZWFyY2gubmV3RmlsdGVyXTtcbiAgICAgICAgdmFyIGZpbHRlck9iamVjdCA9IHtcbiAgICAgICAgICAgIC8vIGZpcmViYXNlIGRvZXNuJ3QgYWxsb3cgdW5kZWZpbmVkIHZhbHVlc1xuICAgICAgICAgICAgbmFtZTogJHNjb3BlLnNlYXJjaC5uZXdGaWx0ZXIsXG4gICAgICAgICAgICBsaW5rOiAnJyxcbiAgICAgICAgICAgIGVxdWFsaXR5OiAnJyxcbiAgICAgICAgICAgIHZhbHVlOiBmaWx0ZXJPcHRpb25zLnZhbHVlIHx8ICcnLFxuICAgICAgICAgICAgdHlwZTogZmlsdGVyT3B0aW9ucy50eXBlIHx8ICcnLFxuICAgICAgICAgICAgbGFiZWw6IGZpbHRlck9wdGlvbnMubGFiZWwgfHwgJycsXG4gICAgICAgICAgICBvcHRpb25zOiBmaWx0ZXJPcHRpb25zLm9wdGlvbnMgfHwgJycsXG4gICAgICAgIH07XG4gICAgICAgICRzY29wZS5zZWFyY2guZmlsdGVycy5wdXNoKGZpbHRlck9iamVjdCk7XG4gICAgfTtcblxuICAgIC8vIHZhciBzZWFyY2ggPSAkZmlyZWJhc2VPYmplY3Qoc2VhcmNoUmVmKTtcblxuICAgIC8vICBzZWFyY2guJGJpbmRUbygkc2NvcGUsICdzZWFyY2gnKTsgLy8gMyB3YXkgYmluZGluZyAobm8gc2F2ZSByZXF1aXJlZClcblxuICAgIC8vICAkc2NvcGUuc2VhcmNoID0gc2VhcmNoO1xuXG4gICAgLy8gdmFyIGFydGljbGVzID0gZmlyZWJhc2UuZGF0YWJhc2UoJ2FydGljbGVzJykucmVmKCk7XG5cbiAgICAvLyAkc2NvcGUuYXJ0aWNsZXMgPSAkZmlyZWJhc2VBcnJheShhcnRpY2xlcyk7XG5cbn1cbiIsIi8qIGdsb2JhbCBhbmd1bGFyICovXG5cbmFuZ3VsYXIubW9kdWxlKCdzZWFyY2gnKS5maWx0ZXIoJ3dlYmhvc2UnLCBzZWFyY2hGaWx0ZXIpO1xuXG5mdW5jdGlvbiBzZWFyY2hGaWx0ZXIoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKHdlYmhvc2VQYXJhbWV0ZXIpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB3ZWJob3NlUGFyYW1ldGVyID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgdmFyIGZpbHRlcmVkID0gd2ViaG9zZVBhcmFtZXRlcjtcbiAgICAgICAgICAgIGZpbHRlcmVkID0gZmlsdGVyZWQucmVwbGFjZSgvXFwuL2csICcgJyk7XG4gICAgICAgICAgICBmaWx0ZXJlZCA9IGZpbHRlcmVkLnJlcGxhY2UoL18vZywgJyAnKTtcbiAgICAgICAgICAgIGZpbHRlcmVkID0gZmlsdGVyZWQucmVwbGFjZSgvc29jaWFsL2ksICcnKTtcbiAgICAgICAgICAgIGZpbHRlcmVkID0gZmlsdGVyZWQucmVwbGFjZSgvcGVyc29ucy9nLCAncGVvcGxlJyk7XG4gICAgICAgICAgICByZXR1cm4gZmlsdGVyZWQudHJpbSgpO1xuICAgICAgICB9XG4gICAgfTtcbn1cbiJdfQ==
