### Fireborn

#### Front-end dev:

- Setup: `cd client && npm run setup`
- Start gulp: `gulp watch`
- Unit Tests: `npm run unit`
- Integration Tests: `npm run integration`
- End-to-end (system) Tests: `npm run e2e`

#### Back-end dev:

- Setup: `cd firebase/functions && npm run setup`
- Start Server: `npm run server`
- Unit Tests: `npm run unit`
- Integration Tests: `npm run integration`
- Functional Tests: `npm run functional`

---

### CoreNLP Service

#### Service Setup
    
    - Directory: core-nlp
    - Run gcp.ce.corenlp.instance.template.cmd
    - Run gcp.ce.corenlp.instance.group.cmd
    - Run gcp.ce.corenlp.dns.cmd
    
#### Update CoreNLP in running container:
    - `docker exec -t corenlp bash -c "$UPDATE" && docker restart corenlp`