/* global angular, firebase */

var config = {
apiKey: "AIzaSyBC88KArmN27_iwYmimElbugz-9Y_401JU",
authDomain: "inbound-planet-218422.firebaseapp.com",
databaseURL: "https://inbound-planet-218422.firebaseio.com",
projectId: "inbound-planet-218422",
storageBucket: "inbound-planet-218422.appspot.com",
messagingSenderId: "41168955520"
};

firebase.initializeApp(config);

angular.module('app', ['firebase','templates', 'search']);
