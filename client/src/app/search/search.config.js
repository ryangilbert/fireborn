/* global angular */

angular.module('search')

    .config(['$routeProvider', ModuleConfig]);

function ModuleConfig($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'search.html',
            controller: 'SearchController',
        });

}
