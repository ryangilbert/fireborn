/* global angular */

angular.module('search').filter('webhose', searchFilter);

function searchFilter() {
    return function(webhoseParameter) {
        if (typeof webhoseParameter === 'string') {
            var filtered = webhoseParameter;
            filtered = filtered.replace(/\./g, ' ');
            filtered = filtered.replace(/_/g, ' ');
            filtered = filtered.replace(/social/i, '');
            filtered = filtered.replace(/persons/g, 'people');
            return filtered.trim();
        }
    };
}
