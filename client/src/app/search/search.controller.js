/* global angular, firebase */

angular.module('search')
    .controller('SearchController', ['$scope', '$firebaseAuth', '$firebaseObject', '$firebaseArray', 'Filters', SearchController]);

function SearchController($scope, $firebaseAuth, $firebaseObject, $firebaseArray, Filters) {

    var searchRef = firebase.database().ref('searches');

    var searches = $firebaseArray(searchRef);
 
    $scope.searches = searches;

    var searchDebounce = false;
    $scope.$watch('search', saveSearch, true);
    
    $scope.articles = [];

    function saveSearch(newValue, oldValue) {
        // if (!searchDebounce) {
        //     searchDebounce = true;
        //     setTimeout(function() {
        //         searchDebounce = false;
        //     }, 5 * 1000);
        if (!oldValue || newValue !== oldValue) {
            if (!newValue) {
                $scope.articles = [];
            }
            if (!oldValue || newValue.articles !== oldValue.articles) {
                $scope.articles = [];
                var articles = firebase.database().ref('articles');
                angular.forEach(newValue.articles, function(articleId) {
                    var articleRef = $firebaseObject(articles.child(articleId));
                    articleRef.$loaded(function(article) {
                        console.log('article', article);
                        $scope.articles.push(article);
                    }, function(err) {
                        console.log(err);
                    });
                });
            }
            return searches.$save($scope.search);
        }
    }

    $scope.options = Filters;

    $scope.createSearch = function() {
        searches.$add({
            name: 'New search ' + (searches.length + 1),
            //from: 0,
            order: '',
            filters: [],
            size: 100,
            sort: '',
        }).then(function() {
            $scope.search = searches[searches.length - 1];
        });
    };

    $scope.addFilter = function() {
        if (!$scope.search.filters) {
            $scope.search.filters = [];
        }
        var filterOptions = $scope.options.filters[$scope.search.newFilter];
        var filterObject = {
            // firebase doesn't allow undefined values
            name: $scope.search.newFilter,
            link: '',
            equality: '',
            value: filterOptions.value || '',
            type: filterOptions.type || '',
            label: filterOptions.label || '',
            options: filterOptions.options || '',
        };
        $scope.search.filters.push(filterObject);
    };

    // var search = $firebaseObject(searchRef);

    //  search.$bindTo($scope, 'search'); // 3 way binding (no save required)

    //  $scope.search = search;

    // var articles = firebase.database('articles').ref();

    // $scope.articles = $firebaseArray(articles);

}
