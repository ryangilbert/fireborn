const jsdom = require('jsdom');
const { JSDOM } = jsdom;
var dom = new JSDOM('');

window = dom.window;
document = dom.window.document;

// mocha and methods required for angular-mocks
window.mocha = {};
window.beforeEach = beforeEach;
window.afterEach = afterEach;

require('angular/angular');
global.angular = window.angular;
require('angular-mocks/angular-mocks');

var unitTests = {
    // module: [submodules]
    search: [
        'search.controller',
        'search.service',
        //'highlight.service'
    ]
};

var appPath = '../src/app/';

for (var moduleName in unitTests) {
    var tests = unitTests[moduleName];
    var modulePath = appPath + moduleName + '/';
    require(modulePath + moduleName + '.module');
    tests.forEach(submodule => {
        var filePath = modulePath + submodule;
        var testPath = filePath + '.unit';
        require(filePath);
        require(testPath);
    });
}
