/* global browser */

const assert = require('assert');

describe('Search', function() {

    beforeEach(function() {
        browser.get('/');
    });

    it('should load the page', function() {
        const ip = process.env.ip || '0.0.0.0';
        const port = process.env.port || 8080;
        const address = ip + port;
        browser.get(address);
    });

});
