exports.config = {
    allScriptsTimeout: 1000 * 30 * 3,
    baseUrl: 'http://0.0.0.0:8080',
    directConnect: true,
    specs: ['test/e2e.js'],
    mochaOpts: {
        slow: 1000 * 8,
        ui: 'bdd',
        timeout: 1000 * 30
    }
};
