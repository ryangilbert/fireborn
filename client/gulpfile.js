var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var iife = require('gulp-iife');
var templateCache = require('gulp-angular-templatecache');
var distDirectory = '../firebase/hosting';

gulp.task('default', ['app', 'css', 'vendor', 'index']);

gulp.task('watch', function() {

	gulp.watch([
		'src/**',
	], ['app', 'css', 'index']);

});

gulp.task('app', function() {

	gulp.src([
			'src/app/**/*.module.js',
			'src/app/**/*.js',
			// negation must be last
			'!src/app/**/*.unit.js',
			'!src/app/**/*.integration.js',
			'!src/app/**/*.css',
			'!src/app/**/*.html'
		])
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(iife())
		.pipe(concat('app.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(distDirectory))
		.on('error', handleError);

	gulp.src('src/app/**/*.html')
		.pipe(templateCache('templates.js', {
			standalone: true,
			moduleSystem: 'IIFE',
			transformUrl: url => {
				const fileNameIndex = url.lastIndexOf('/') + 1;
				const noPath = url.substr(fileNameIndex);
				return noPath;
			}
		}))
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(gulp.dest(distDirectory));

});

gulp.task('css', function() {

	return gulp.src([
			'./node_modules/normalize.css/normalize.css',
			'src/app/shared/app.css',
			'src/**/*.css',
		])
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(concat('app.css'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(distDirectory))
		.on('error', handleError);

});

gulp.task('vendor', function() {
	return gulp.src([
			'./node_modules/angular/angular.js',
			'./node_modules/angular-route/angular-route.js',
			'./node_modules/firebase/firebase.js',
			'./node_modules/firebase/firebase-firestore.js',
			'./node_modules/ngstorage/ngStorage.js',
			'./node_modules/angular-sanitize/angular-sanitize.js',
			'./node_modules/ng-csv/build/ng-csv.js',
			'./node_modules/angularfire/dist/angularfire.js',
		])
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(concat('vendor.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(distDirectory))
		.on('error', handleError);
});

gulp.task('index', function() {
	return gulp.src([
			'./src/index.html',
		])
		.pipe(gulp.dest(distDirectory))
		.on('error', handleError);
});

function handleError(error) {
	this.emit('end');
	throw error;
}